﻿using UnityEngine;
using System.Collections;

public class Background_NodeSystem : MonoBehaviour {

	public float yStartMod;
	private Vector3 startPos;
	private Vector3 endPos;
	private GameObject followObject;
	private float curPerc;

	// Use this for initialization
	void Start () {
		transform.position = GameObject.Find("Level Start Point").transform.position + new Vector3(0f, -yStartMod, 0f);
		startPos = GameObject.Find("Level Start Point").transform.position;
		endPos = GameObject.Find("Level End Point").transform.position;
		followObject = GameObject.Find("Runner Player 1");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		curPerc = 1-followObject.transform.position.x/endPos.x;
		Vector3 curPos = transform.position;
		curPos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width*curPerc, Screen.height/2, 10f));
		//curPos.x = Mathf.SmoothDamp(transform.position.x, curPos.x, ref followObject.rigidbody2D.velocity.x, 0.1f );


		transform.position = curPos;
	}
}
