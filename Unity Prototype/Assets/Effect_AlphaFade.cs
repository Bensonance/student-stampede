﻿	using UnityEngine;
using System.Collections;

public class Effect_AlphaFade : MonoBehaviour {

	public bool startOff = false;
	public float decSpeed;
	public float curAlpha;

	// Use this fsdzor initialization
	void Start () {
		if(startOff){
			curAlpha = 0f;

		}else{
			curAlpha = 1f;
			decSpeed *= -1;
		}
	}
	
	// Update is called once per frame
	void Update () {
		curAlpha += decSpeed;
		curAlpha = Mathf.Clamp(curAlpha, 0f, 1f);

		Color col = GetComponent<SpriteRenderer>().color;
		col.a = curAlpha;
		GetComponent<SpriteRenderer>().material.color = col;
	}
}
