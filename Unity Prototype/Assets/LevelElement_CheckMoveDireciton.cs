﻿using UnityEngine;
using System.Collections;

public class LevelElement_CheckMoveDireciton : MonoBehaviour {

	public int desiredMoveDir; //1 == right; -1 == left

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col){
		if(col.tag == "Player" && Mathf.Sign(col.rigidbody2D.velocity.x) != desiredMoveDir){
			col.rigidbody2D.velocity = new Vector2(col.rigidbody2D.velocity.x *-1, col.rigidbody2D.velocity.y);
			col.GetComponent<RunnerMovement>().moveMod*=-1;
		}
	}
}
