﻿using UnityEngine;
using System.Collections;

public class LevelElement_SectionMercy : MonoBehaviour {

	public int promptMercyNo;
	public int currentFails;
	public bool canPrompt = false;

	public GameObject fallResetAreaObjects;

	public GameObject promptObj;
	public float guiSizeMod;
	private string guiTextDisply;
	// Use this for initialization
	void Start () {
		promptObj.transform.position = Vector3.zero;
		promptObj.guiText.fontSize = Mathf.RoundToInt(Screen.width * guiSizeMod);
		promptObj.guiText.pixelOffset = new Vector2(Screen.width/2, Screen.height/1.7f);
		promptObj.guiText.text = "Struggling?\n \n Press Alt to skip this section for later,\n \n Press space to keep going";
		promptObj.guiText.enabled = false;

	}
	
	// Update is called once per frame
	void Update () {
		if(canPrompt && currentFails >= promptMercyNo)
		{
			promptObj.guiText.enabled = true;
			Time.timeScale = 0;

			if(Input.GetKeyUp(KeyCode.LeftAlt) || Input.GetKeyUp(KeyCode.RightAlt))BoostForward();
			if(Input.GetKeyUp(KeyCode.Space))KeepTrying();
		}
	}

	void BoostForward(){
		GameObject.Find("Runner Player 1").transform.position = fallResetAreaObjects.transform.GetChild(0).position;
		Camera.main.transform.position = fallResetAreaObjects.transform.GetChild(1).position;

		promptObj.guiText.enabled = false;
		Time.timeScale = 1;
		canPrompt = false;


		/*Vector3 curVec = cameraWaitPoint.transform.position;
		curVec.z = Camera.main.transform.position.z;
		iTween.MoveTo(Camera.main.gameObject, curVec, 1f);*/

	}
	
	void KeepTrying()
	{
		promptObj.guiText.enabled = false;
		Time.timeScale = 1;
		canPrompt = false;
	}
}
