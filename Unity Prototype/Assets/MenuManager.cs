﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {

	public GameObject blackFade;
	private GameObject fade;

	public GameObject char1;
	public GameObject char2;
	public GameObject char3;
	private bool hasCharactered = false;
	private GameObject[] characters = new GameObject[3];
	private int curIndex = 0;
	public GameObject animatorTracker;
	public RuntimeAnimatorController[] rr;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Space)){
			fade = (GameObject)Instantiate(blackFade, Camera.main.transform.position+new Vector3(0f, 0f, 10f), transform.rotation);
			LevelEndPoint lvp = GameObject.Find("Level End Point").GetComponent<LevelEndPoint>();
			Destroy(lvp.newLogo);
			Destroy(lvp.loadPro);
		}

		if(fade != null && fade.GetComponent<Effect_AlphaFade>().curAlpha >= 1 && !hasCharactered){
			characters[0] = (GameObject) Instantiate(char1, Camera.main.ScreenToWorldPoint(new Vector3(0f, Screen.height, 0f))+new Vector3(0f, 0f, 9f), transform.rotation);
			characters[1] = (GameObject) Instantiate(char2, Camera.main.ScreenToWorldPoint(new Vector3(Screen.width/2, Screen.height, 0f))+new Vector3(0f, 0f, 9f), transform.rotation);
			characters[2] = (GameObject) Instantiate(char3, Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0f))+new Vector3(0f, 0f, 9f), transform.rotation);
		
			fade.GetComponent<Effect_AlphaFade>().decSpeed *= -1;
			hasCharactered = true;
		}

		for(int i = 0; i < characters.Length; i++){
			if(characters[i] != characters[curIndex]){
				characters[i].GetComponent<SpriteRenderer>().material.color = Color.grey;
			}else{
				characters[i].GetComponent<SpriteRenderer>().material.color = Color.white;
			}
		}

		if(hasCharactered && Input.GetKeyUp(KeyCode.Space)){
			GameObject newObj = (GameObject)Instantiate(animatorTracker, transform.position, transform.rotation);
			newObj.GetComponent<Utility_StoreAnimatorController>().playerChosenChar = rr[curIndex];
			Application.LoadLevel(Application.loadedLevel+1);
		}

		if(Input.GetKeyUp(KeyCode.LeftArrow)){
			curIndex--;
		}else if(Input.GetKeyUp(KeyCode.RightArrow)){
			curIndex++;
		}

		if(curIndex <0) curIndex = characters.Length-1;
		if(curIndex >= characters.Length) curIndex = 0;
	}
}
