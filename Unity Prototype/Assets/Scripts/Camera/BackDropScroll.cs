﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Author: Dirk 
/// Back drop scroll.
/// </summary>
public class BackDropScroll : MonoBehaviour {

	public float verticleSpeed = 1.0f;
	public float minHorizontleSpeed = 1.0f;
	public float maxHorizontalSpeed = 3.0f;
	public float maxYOffset = 3.0f;
	public float maxXOffset = 3.0f;

	public GameObject parralexWithGameObject;
	public string targetName = "PlayerCamera"; 
	public float fudgeFactor = 0.2f;
	
	private float offsetY = 0;
	private float offsetX = 0;

	private float startY;

	//Debug
	public bool debugScrolling;

	void Start ()
	{
		offsetX = renderer.material.GetTextureOffset("_MainTex").x;
		offsetY = renderer.material.GetTextureOffset("_MainTex").y;
		startY = offsetY;

		if (parralexWithGameObject == null)
		{
			parralexWithGameObject = GameObject.Find(targetName);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{

		if (parralexWithGameObject == null)
		{
			parralexWithGameObject = GameObject.Find(targetName);
		}

		//if (parralexWithGameObject == null)
		{
			offsetY = startY +(parralexWithGameObject.transform.position.y * fudgeFactor);
			offsetX = offsetX + CalculateHorizontalScrollSpeed() * Time.deltaTime;
		}
		/*else
		{
			offsetX = parralexWithGameObject.transform.position.x * fudgeFactor;		
			offsetY = startY +(parralexWithGameObject.transform.position.y * fudgeFactor);
		}*/

		// This insure that there isn't errors when offsetY goes too high
		if (Mathf.Abs(offsetY) > maxYOffset)
		{
			offsetY = startY;
		}

		// This insures that there isn't errors when offsetX goes too high
		if (Mathf.Abs(offsetX) > maxXOffset)
		{
			//offsetX = 0;
		}
		DebugStuff();

		// move the texture
		renderer.material.SetTextureOffset("_MainTex", new Vector2(offsetX, offsetY));

	}

	float CalculateHorizontalScrollSpeed(){

		float percSpeed = parralexWithGameObject.rigidbody2D.velocity.x / parralexWithGameObject.GetComponent<RunnerMovement>().maxMoveSpeed;
		float ratio = percSpeed *maxHorizontalSpeed;
		ratio = Mathf.Clamp(ratio, minHorizontleSpeed, maxHorizontalSpeed);
		return ratio;
	}

	void DebugStuff(){
		if(debugScrolling)Debug.Log("Hscroll Speed: " + CalculateHorizontalScrollSpeed());
	}
}
