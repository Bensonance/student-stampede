﻿using UnityEngine;
using System.Collections;

public class Camera_ResizeForSection : MonoBehaviour {

	public float cameraSizeForSection;
	private SinglePlayerCamera playerCam; 
	public float exitReturnToOriginalSizeDelay;
	private Vector3 startOffset;
	public Vector3 sectionOffset;
	private float cameraSizeStart;
	public bool followPlayer;

	public bool resetDynamism;

	public float resizeCameraSizeTime;

	public bool isTweening= false;
	public bool mustDestroy = false;

	// Use this for initialization
	void Start () {
		playerCam = Camera.main.gameObject.GetComponent<SinglePlayerCamera>();
	}
	
	// Update is called once per frame
	void Update () {
		//if(isTweening) playerCam.offset = iTween.Vector3Update(playerCam.offset, sectionOffset, 0.1f);	
	}

	#region Collisions
	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "Player")
		{
			playerCam.canDynamicallyResize = false;
			cameraSizeStart = playerCam.camera.orthographicSize;
			iTween[] tweens = playerCam.gameObject.GetComponents<iTween>();

			foreach(iTween t in tweens){
				Destroy(t);
			}

			playerCam.StopAllCoroutines();
			playerCam.ChangeCameraSize(cameraSizeForSection, iTween.EaseType.easeOutBounce, 0.1f);
			startOffset = playerCam.offset;
			if(!followPlayer) playerCam.followedObject = gameObject;
			playerCam.offset = sectionOffset;
			//isTweening = true;

		}
	}

	void OnTriggerStay(Collider2D col){
		if(col.tag == "Player")
		{
			playerCam.canDynamicallyResize = false;
			playerCam.offset = sectionOffset;
			if(!followPlayer) playerCam.followedObject = gameObject;
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		if(col.tag == "Player")
		{
			playerCam.offset = startOffset;
			playerCam.ChangeCameraSize(cameraSizeStart, iTween.EaseType.easeInExpo, resizeCameraSizeTime);
			playerCam.followedObject = col.gameObject;
			if(resetDynamism)StartCoroutine(playerCam.DynamicCameraDelay(1.1f));
			isTweening = false;
			if(mustDestroy) Destroy(gameObject);
		}
	}
	#endregion Collisions
}
