﻿using UnityEngine;
using System.Collections;

public class ChangeCameraSize : MonoBehaviour {

	[HideInInspector] public float cameraStartSize = 0;
	public float resetCameraSize; 
	private float currentCameraSize;

	// Use this for initialization
	void Start () {
		cameraStartSize = camera.orthographicSize;
		currentCameraSize = cameraStartSize;
	}
	
	// Update is called once per frame
	void Update () {
		camera.orthographicSize = currentCameraSize;
	}

	public void ChangeCameraSizeOrtho(float newSize, iTween.EaseType easeType){
		iTween.ValueTo( gameObject, iTween.Hash(
			"from", currentCameraSize,
			"to", newSize,
			"time", 0.3f,
			"onupdatetarget", gameObject,
			"onupdate", "tweenOnUpdateCallBack",
			"easetype", easeType
			)
		               );
	}

	void tweenOnUpdateCallBack( int newValue )
	{
		currentCameraSize = newValue;
	}
}
