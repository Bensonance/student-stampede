﻿using UnityEngine;
using System.Collections;

public class SinglePlayerCamera : MonoBehaviour {

	public GameObject followedObject;
	public string followedObjectName;
	private Vector3 velocity = Vector3.zero;
	public float dampTime = 0.15f;
	public Vector3 offset;

	/*Screenshake*/
	public bool isShaking;
	public float shakeIntensity;
	public Vector3 shakeOffset;
	private float curShakeTime;

	/*States*/
	[HideInInspector] public bool isResetting = false;
	[HideInInspector] public GameObject resetTarget;

	float exampleInt = 0;

	[HideInInspector] public float cameraStartSize = 0;
	public float resetCameraSize; 
	private float currentCameraSize;
	private bool cameraDramaticallyChanging;

	/*Dynamic Sizing System*/
	public float minCameraSize;
	public float maxCameraSize;
	public float maxSpeedChangeAllowed;
	private Rigidbody2D _playerMover;
	public bool canDynamicallyResize;
	public float dynamicSystemResetDelay;

	private float startXOffset;


	private float percOfScreen;

	/*Reset area hacks*/
	public GameObject sectionTarget;
	public AudioClip fallResetClip;
	public float fallResetVolume;

	/*Debug*/
	public bool testingUpClose;
	// Use this for initialization
	void Start () {
		if(testingUpClose){
			canDynamicallyResize = false;
			camera.orthographicSize = minCameraSize;
		}

		followedObject = GameObject.Find(followedObjectName);
		cameraStartSize = camera.orthographicSize;
		currentCameraSize = cameraStartSize;
		_playerMover = GameObject.FindGameObjectWithTag("Player").rigidbody2D;
		startXOffset = offset.x;
		percOfScreen = startXOffset/Camera.main.orthographicSize;

			
	}
	
	// Update is called once per frame
	void Update () {

		if(_playerMover == null)_playerMover = followedObject.rigidbody2D;

		if(isShaking){
			shakeOffset = Vector3.zero;
			shakeOffset = new Vector3(Random.Range(-shakeIntensity, shakeIntensity), Random.Range(-shakeIntensity, shakeIntensity), 0f);
			curShakeTime -= Time.deltaTime;
			if(curShakeTime <= 0) isShaking = false;	
		}else{
			shakeOffset = Vector3.zero;
		}

		float yVel = 0.0f;
		if(canDynamicallyResize)offset.x =  Mathf.SmoothDamp(offset.x, percOfScreen*Camera.main.orthographicSize, ref yVel,  0.05f);

		Vector3 point = camera.WorldToViewportPoint(followedObject.transform.position);
		Vector3 delta = (followedObject.transform.position +offset)- camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
		Vector3 destination = transform.position + delta;

		if(!isResetting){
			transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime)+shakeOffset;
		}else if((Vector3.Distance(transform.position, resetTarget.transform.position) <= 0.5f || transform.position.x == resetTarget.transform.position.x)&& !followedObject.GetComponent<StateTracker>().canReset){
			if(sectionTarget != null){
				sectionTarget.GetComponent<LevelElement_SectionMercy>().canPrompt = true;
				sectionTarget = null;
			}
			followedObject.GetComponent<StateTracker>().canReset = true;
			ChangeCameraSize(resetCameraSize, iTween.EaseType.easeOutBounce, 0.5f);

			//Sound reset
			audio.clip = fallResetClip;
			audio.volume = fallResetVolume;
			audio.Play();
		}

		camera.orthographicSize = currentCameraSize;
		if(canDynamicallyResize && !cameraDramaticallyChanging && !isResetting)CameraSizeSpeedRatio();
		cameraDramaticallyChanging = false;

		Vector3 curVec = transform.position;
		curVec.z = -10;
		transform.position = curVec;

		DebugControls();
	}

	public void ShakeScreen(float shakeVar, float period){
		if(!isShaking){
			isShaking = true;
			shakeIntensity = shakeVar;
			curShakeTime = period;
		}
	}


	#region CameraSizeChange
	public void ChangeCameraSize(float newSize, iTween.EaseType easeType, float changeTime){
		iTween.ValueTo( gameObject, iTween.Hash(
			"from", currentCameraSize,
			"to", newSize,
			"time", changeTime,
			"onupdatetarget", gameObject,
			"onupdate", "tweenOnUpdateCallBack",
			"easetype", easeType
			)
		);
	}
	

	void tweenOnUpdateCallBack( int newValue )
	{
		currentCameraSize = newValue;
		//camera.orthographicSize = currentCameraSize;
		cameraDramaticallyChanging = true;
	}

	void CameraSizeSpeedRatio(){
		float speedPerc = Mathf.Abs(_playerMover.velocity.x) / Mathf.Abs(_playerMover.gameObject.GetComponent<RunnerMovement>().maxMoveSpeed);
		float ratio = speedPerc * maxCameraSize;
		ratio = Mathf.Clamp(ratio, minCameraSize, maxCameraSize);
		float yvel = 0.0f;
		camera.orthographicSize = Mathf.SmoothDamp(camera.orthographicSize, ratio, ref yvel, 0.001f);
	}
	#endregion CameraSizeChange

	#region Timers
	public IEnumerator DynamicCameraDelay(){
		yield return new WaitForSeconds(dynamicSystemResetDelay);
		canDynamicallyResize = true;
	}

	public IEnumerator DynamicCameraDelay(float period){
		yield return new WaitForSeconds(period);
		canDynamicallyResize = true;
	}
	#endregion Timers

	void DebugControls(){
		if(Input.GetKeyUp(KeyCode.G)){
			canDynamicallyResize = !canDynamicallyResize;

		}
	}
}

