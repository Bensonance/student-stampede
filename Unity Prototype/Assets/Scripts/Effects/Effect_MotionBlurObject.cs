﻿using UnityEngine;
using System.Collections;

public class Effect_MotionBlurObject : MonoBehaviour {

	public int blurNo;
	public float blurUpdate;
	private float curTime = 0;
	public GameObject blurObject;
	public GameObject effectBlurObject;
	public float alphaDecTime;
	public float startAlpha;
	public bool trackObject;
	public bool matchObjectSpeed;
	public float speedDiff;
	// Use this for initialization
	void Start () {
		curTime = blurUpdate;
		blurObject = GameObject.Find("Runner Player 1");
	}
	
	// Update is called once per frame
	void Update () {
		if(curTime >0){
			curTime-=Time.deltaTime;
		}else{
			curTime = blurUpdate;
			if(GameObject.FindGameObjectsWithTag("Blurs").Length <= blurNo){
				GameObject newBlur = (GameObject) Instantiate(effectBlurObject, blurObject.transform.position, transform.rotation);
				newBlur.transform.localScale = blurObject.transform.localScale;
				newBlur.GetComponent<SpriteRenderer>().sprite = blurObject.GetComponent<SpriteRenderer>().sprite;
				newBlur.GetComponent<TextFadeIn>().fadeTime = alphaDecTime;
				Color w = Color.white;
				w.a = startAlpha;
				newBlur.GetComponent<SpriteRenderer>().material.color = w;
				if(trackObject)newBlur.GetComponent<TextFadeIn>().trackingObj = blurObject;
				//newBlur.GetComponent<TextFadeIn>().StartFade();
			}
		}
	}

	void FixedUpdate(){
		if(matchObjectSpeed){
			GameObject[] blurs = GameObject.FindGameObjectsWithTag("Blurs");
			foreach(GameObject g in blurs){
				Vector2 desiredSpeed = blurObject.rigidbody2D.velocity;
				desiredSpeed.x -= speedDiff;
				g.rigidbody2D.velocity = desiredSpeed;
			}
		}
	}
}
