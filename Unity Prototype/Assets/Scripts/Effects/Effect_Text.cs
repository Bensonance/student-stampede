﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (TextMesh))]
public class Effect_Text : MonoBehaviour {

	public Color textColour;
	public float textLife;

	public string textDrawn;

	public bool mustFloat;
	public float floatSpeed;

	public bool mustFade;
	public float fadeSpeed;

	public bool mustScaleSize;
	public int scaleSizeSpeed;

	private TextMesh textDrawer;
	private float currentTime;

	// Use this for initialization
	public void Setup (Color tempColour, float tempLife, string tempString, bool floatUp, 
	            float floatingSpeed, bool fadeMust, bool mustScale, int scaleSpeed, int startFontSize) {
		transform.rotation = new Quaternion(0,0,0,0);

		textColour = tempColour;
		textColour.a = 1;
		textLife = tempLife;
		textDrawn = tempString;

		mustFloat = floatUp;
		floatSpeed = floatingSpeed;

		mustFade = fadeMust;
		mustScaleSize = mustScale;
		scaleSizeSpeed = scaleSpeed;

		textDrawer = GetComponent<TextMesh>();
		textDrawer.text = textDrawn;
		textDrawer.color = tempColour;
		currentTime = textLife;
		GetComponent<TextMesh>().fontSize = startFontSize;
		if(mustFade)fadeSpeed = 1/textLife;
		//StartCoroutine(DestroyCountdown(textLife));
	}
	
	// Update is called once per frame
	void Update () {
		if(mustFloat) transform.Translate(Vector3.up * floatSpeed);
		if(mustFade){
			textColour.a = currentTime/textLife;
			textDrawer.renderer.material.color = textColour;
		}

		if(mustScaleSize){
			GetComponent<TextMesh>().fontSize += scaleSizeSpeed;
		}

		if(currentTime >= 0){
			currentTime-= Time.deltaTime;
		}else{
			GameObject.Destroy(gameObject);
		}
	}

	IEnumerator DestroyCountdown(float period){
		yield return new WaitForSeconds(period);
		GameObject.Destroy(gameObject);
	}
}
