﻿using UnityEngine;
using System.Collections;

public class FakeParticleControl : MonoBehaviour {

	public Vector2 speedRangeX;
	public Vector2 speedRangeY;

	public float speedAccX;
	public float speedAccY;
	private float speed;

	public Vector2 lifeRange;
	private float currentLife;
	// Use this for initialization
	void Start () {
		rigidbody2D.velocity = new Vector2(Random.Range(speedRangeX.x, speedRangeX.y), Random.Range(speedRangeY.x, speedRangeY.y));
		currentLife = Random.Range(lifeRange.x, lifeRange.y);
	}
	
	// Update is called once per frame
	void Update () {
		if(currentLife <= 0){
			Destroy(gameObject);
		}else{
			currentLife -= Time.deltaTime;
		}

		rigidbody2D.velocity += new Vector2(speedAccX, speedAccY);

		Vector3 curVel = rigidbody2D.velocity;
		if(curVel.x > speedRangeX.y)curVel.x = speedRangeX.y;
		if(curVel.y > speedRangeY.y)curVel.y = speedRangeY.y;
		rigidbody2D.velocity = curVel;
	}
}
