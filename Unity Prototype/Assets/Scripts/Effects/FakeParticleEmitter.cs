﻿using UnityEngine;
using System.Collections;

public class FakeParticleEmitter : MonoBehaviour {

	public GameObject particle;
	public int minParticlesPerStep;
	public int maxParticlesPerStep;
	private StateTracker _state;

	public float xOffset;

	// Use this for initialization
	void Start () {
		_state = GetComponent<StateTracker>();
	}
	
	// Update is called once per frame
	void Update () {
		if(_state.onGround && _state.moving){
			for(int i = 0; i <GenerateParticleRatio(); i++){
				GameObject newPart = (GameObject) Instantiate(particle, transform.position + new Vector3(xOffset, 0f, 0f), transform.rotation);
				newPart.rigidbody2D.velocity = new Vector2(newPart.rigidbody2D.velocity.x*-1f, newPart.rigidbody2D.velocity.y);
				newPart.GetComponent<FakeParticleControl>().speedAccX *=-1;
				newPart.transform.parent = GameObject.Find("Effects").transform;
				//newPart.GetComponent<FakeParticleControl>().speedAccY *=-1;
			}
		}
	}

	int GenerateParticleRatio(){
		float perc = rigidbody2D.velocity.x / GetComponent<RunnerMovement>().maxMoveSpeed;
		float ratio = perc * maxParticlesPerStep;
		ratio = Mathf.Clamp(ratio, minParticlesPerStep, maxParticlesPerStep);
		return Mathf.RoundToInt(ratio);
	}
}
