﻿using UnityEngine;
using System.Collections;

public class TextFadeIn : MonoBehaviour {

	public float startAlpha;
	public float goalAlpha;
	public float fadeTime;
	public bool onStart;
	public bool mustTween = false;
	public bool manualDec;
	public GameObject trackingObj;
	public bool spriteRenderer;

	public float maxFadeDec;
	public float minFadeDec;
	private GameObject playerObj;
	Color white;

	// Use this for initialization
	void Start () {
		if(onStart)mustTween = true;
		white = Color.white;
		playerObj = GameObject.Find("Runner Player 1");

	}
	
	// Update is called once per frame
	void Update () {

		if(playerObj== null) playerObj = GameObject.Find("Runner Player 1");

		if(mustTween)iTween.FadeTo(gameObject, goalAlpha, fadeTime);

		if(!manualDec && (GetComponent<GUIText>() != null && guiText.color.a <= goalAlpha ) || (renderer != null && renderer.material.color.a <= goalAlpha )){
			Destroy(gameObject);
		}else if(manualDec){
//			Debug.Log("Decreasing: " + white.a);

			white.a -=  CalculateRatio();
			if(spriteRenderer){
				GetComponent<SpriteRenderer>().material.color = white;
			}else{
				renderer.material.color = white;
			}

			if(renderer.material.color.a <= 0f) Destroy(gameObject);
		}

		if(trackingObj != null && Vector2.Distance(transform.position, trackingObj.transform.position) >= 10f)Destroy(gameObject);
	}

	public void StartFade(){
		mustTween = true;
	}

	float CalculateRatio(){
		float curSpeed = playerObj.rigidbody2D.velocity.x/ playerObj.GetComponent<RunnerMovement>().maxMoveSpeed;
		float transSpeed = curSpeed *maxFadeDec;
		if(transSpeed <= minFadeDec) transSpeed = minFadeDec;
		return transSpeed;
	}
}
