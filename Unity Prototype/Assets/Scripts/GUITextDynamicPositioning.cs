﻿using UnityEngine;
using System.Collections;

public class GUITextDynamicPositioning : MonoBehaviour {

	public float widthMainMod;
	public float widthPositioningMod;

	public float heightMainMod;
	public float guiTextSizeMod;

	public bool liveUpdating;

	// Use this for initialization
	void Start () {
		guiText.pixelOffset = new Vector2(Screen.width/widthMainMod, Screen.height/heightMainMod);
		guiText.fontSize = Mathf.RoundToInt(Screen.height * guiTextSizeMod);
	}

	void Update(){
		if(liveUpdating){
			guiText.pixelOffset = new Vector2(Screen.width/widthMainMod, Screen.height/heightMainMod);
			guiText.fontSize = Mathf.RoundToInt(Screen.height * guiTextSizeMod);
		}
	}
}
