﻿using UnityEngine;
using System.Collections;

public class LevelElement_Bouncer : MonoBehaviour {

	public float bounceSpeed;
	public float playerNewMaxSpeed;
	public float playerNewSpeedPeriod;
	public float jumpDisallowPeriod;

	public AudioClip bounceSound;
	public float bounceSoundVolume;
	public float bounceSoundPitchVar;
	private float bounceSoundStartPitch;


	// Use this for initialization
	void Start () {
		BounceScale();
		bounceSoundStartPitch = audio.pitch;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "Player"){
			col.GetComponent<RunnerMovement>().IncreaseMaxSpeed(playerNewMaxSpeed, playerNewSpeedPeriod);
			BounceObject(col.gameObject);
		}
	}

	void OnTriggerExit2D(Collider2D col){
		if(col.tag == "Player"){
			GetComponent<Animator>().SetBool("canBounce", false);
		}
	}

	void BounceObject(GameObject bouncedObj){
		bouncedObj.transform.position = transform.position;
		bouncedObj.rigidbody2D.AddForce(transform.up *bounceSpeed);
		bouncedObj.rigidbody2D.velocity = bouncedObj.rigidbody2D.velocity.magnitude*transform.up;
		bouncedObj.GetComponent<RunnerMovement>().ChangeMoveModBasedOnSpeed();
		bouncedObj.GetComponent<RunnerMovement>().canJump = false;
		bouncedObj.GetComponent<RunnerMovement>().DisallowJumpingPeriod(jumpDisallowPeriod);
		bouncedObj.GetComponent<RunnerMovement>().StopCoroutine("DisallowJumpingPeriod");
		GetComponent<Animator>().SetBool("canBounce", true);
		BounceScale();
		audio.clip = bounceSound;
		audio.pitch = bounceSoundStartPitch + Random.Range(-bounceSoundPitchVar, bounceSoundPitchVar);
		audio.volume = bounceSoundVolume;
		audio.Play();
	}

	void BounceScale(){
		iTween.ShakeScale(gameObject, new Vector3(0f, 0.5f, 0f), 0.5f);
	}
}
