﻿using UnityEngine;
using System.Collections;

public class LevelElement_BouncyFloor : MonoBehaviour {

	public float notDivingBounceBackSpeed;
	public float divingSpeedMod;

	public AudioClip bounceSound;
	public float bounceAudioVolume;

	public float bounceAudioPitchVar;
	private float bouncePitchStart;

	// Use this for initialization
	void Start () {
		bouncePitchStart = audio.pitch;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "Player" && other.rigidbody2D.velocity.y <0){
			if(other.gameObject.GetComponent<StateTracker>().isDiving){
				Debug.Log("Diving");
				//Debug.Break();
				other.rigidbody2D.velocity = new Vector2(other.rigidbody2D.velocity.x, -(other.rigidbody2D.velocity.y*divingSpeedMod) );
				RunnerMovement playerMove = other.GetComponent<RunnerMovement>();
				playerMove.jumpReleased = false;
			}else{
				other.rigidbody2D.velocity = new Vector2(other.rigidbody2D.velocity.x, notDivingBounceBackSpeed);
			}
			audio.clip = bounceSound;
			audio.volume = bounceAudioVolume;
			audio.pitch = bouncePitchStart + Random.Range(-bounceAudioPitchVar, bounceAudioPitchVar);
			audio.Play();

			//iTween.ShakeScale(gameObject, new Vector3(0f, 0.5f, 0f), 0.5f);
			GetComponent<Animator>().SetBool("canBounce", true);
		}
	}

	void OnTriggerExit2D(Collider2D other){
		if(other.tag == "Player"){
			GetComponent<Animator>().SetBool("canBounce", false);
		}
	}
}
