﻿using UnityEngine;
using System.Collections;

public class LevelElement_Fall : MonoBehaviour {

	public GameObject playerResetPoint;
	public GameObject cameraWaitPoint;
	public GameObject resetCompleteArea;
	public float cameraMoveDelay;
	private GameObject storedObject;
	public GameObject sectionMercyObject;

	public AudioClip fallSound;
	public float fallSoundVolume;


	public bool killHorizontalMovement;
	
	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "Player")
		{
			FallReset(col.gameObject);
		}
	}

	void FallReset(GameObject resetPlayer)
	{
		//Sound
		audio.clip = fallSound;
		audio.volume = fallSoundVolume;
		audio.Play();

		//Camera
		SinglePlayerCamera playerCam = Camera.main.GetComponent<SinglePlayerCamera>();
		playerCam.cameraStartSize = Camera.main.orthographicSize;
		playerCam.isResetting = true;
		cameraWaitPoint.transform.position = new Vector3(playerResetPoint.transform.position.x+ playerCam.offset.x, cameraWaitPoint.transform.position.y, 0f);
		playerCam.resetTarget = cameraWaitPoint;

		playerCam.canDynamicallyResize = false;

		//Player stuff
		resetCompleteArea.GetComponent<LevelElement_FallResetComplete>().storedXSpeed = resetPlayer.rigidbody2D.velocity.x;

		if(killHorizontalMovement)resetPlayer.rigidbody2D.velocity = new Vector2(0f, resetPlayer.rigidbody2D.velocity.y);
		resetPlayer.GetComponent<StateTracker>().isResetting = true;
		resetPlayer.GetComponent<StateTracker>().canReset = false;
		storedObject = resetPlayer;

		if(sectionMercyObject != null){
			sectionMercyObject.GetComponent<LevelElement_SectionMercy>().currentFails++;
			sectionMercyObject.GetComponent<LevelElement_SectionMercy>().canPrompt = false;
			playerCam.sectionTarget = sectionMercyObject;
		}
		//Camera.main.transform.position = cameraWaitPoint.transform.position;
		StartCoroutine(MoveDelay(cameraMoveDelay));
	}

	IEnumerator MoveDelay(float period){
		yield return new WaitForSeconds(period);
		Vector3 curVec = cameraWaitPoint.transform.position;
		curVec.z = Camera.main.transform.position.z;
		iTween.MoveTo(Camera.main.gameObject, curVec, 1f);
		storedObject.rigidbody2D.velocity = Vector3.zero;
		storedObject.rigidbody2D.gravityScale = 0f;
		storedObject.transform.position = playerResetPoint.transform.position;
	}


}
