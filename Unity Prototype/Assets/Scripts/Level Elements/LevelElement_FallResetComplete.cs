﻿using UnityEngine;
using System.Collections;

public class LevelElement_FallResetComplete : MonoBehaviour {

	public float storedXSpeed;
	public float backupXSpeed;

	public AudioClip resetLandSound;
	public float resetLandVolume;

	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "Player" && col.GetComponent<StateTracker>().isResetting)
		{

			//Camera stuff
			SinglePlayerCamera playerCam = Camera.main.GetComponent<SinglePlayerCamera>();
			playerCam.isResetting = false;
			playerCam.ChangeCameraSize(Camera.main.GetComponent<SinglePlayerCamera>().cameraStartSize, iTween.EaseType.easeInOutBack, 0.5f);
			StartCoroutine(playerCam.DynamicCameraDelay());

			//Restore player's speed
			col.GetComponent<StateTracker>().isResetting = false;
			col.rigidbody2D.velocity += new Vector2(storedXSpeed < backupXSpeed ? backupXSpeed: storedXSpeed, 0f);
			storedXSpeed = 0f;

			audio.clip = resetLandSound;
			audio.volume = resetLandVolume;
			audio.Play();

		}
	}
}
