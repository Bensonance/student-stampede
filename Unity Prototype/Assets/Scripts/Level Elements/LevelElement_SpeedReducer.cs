﻿using UnityEngine;
using System.Collections;

public class LevelElement_SpeedReducer : MonoBehaviour {
	
	public bool slowOnEnter;
	public float initialSpeedReduction;
	public float stepSpeedReduction;
	public bool boostOnExit;
	public bool restoreSpeed;
	public float leaveSpeedBoost;
	private bool appliedInitalSlow = false;
	public float initialSlowCooldown;

	private float enterPlayerSpeed;

	public AudioClip enterSound;
	public float enterSoundVolume;

	#region Collisions

	void OnTriggerEnter2D(Collider2D col){
		if(col.tag == "Player"){
			enterPlayerSpeed = col.rigidbody2D.velocity.x;
			if(col.rigidbody2D.velocity.x - initialSpeedReduction >0 && slowOnEnter && !appliedInitalSlow)
			{
				col.rigidbody2D.velocity += new Vector2(-initialSpeedReduction, 0f);
				appliedInitalSlow = true;
				StartCoroutine(SlowDownCooldown(initialSlowCooldown));
				audio.clip = enterSound;
				audio.volume = enterSoundVolume;
				audio.Play();
			}
			col.GetComponent<RunnerMovement>().canAccelerate = false;

			//GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).
		}
	}

	void OnTriggerStay2D(Collider2D col){
		if(col.tag == "Player"){
			if(col.rigidbody2D.velocity.x - stepSpeedReduction >0) col.rigidbody2D.velocity += new Vector2(-stepSpeedReduction, 0f);
		}
	}

	void OnTriggerExit2D(Collider2D col){
		if(col.tag == "Player" && boostOnExit){
			col.rigidbody2D.velocity = new Vector2( restoreSpeed ? enterPlayerSpeed : col.rigidbody2D.velocity.x + leaveSpeedBoost, col.rigidbody2D.velocity.y);
		}
		col.GetComponent<RunnerMovement>().canAccelerate = true;
	}
	
	#endregion Collisions

	#region Timers
	IEnumerator SlowDownCooldown(float period){
		yield return new WaitForSeconds(period);
		appliedInitalSlow = false;
	}
	#endregion Timers
}
