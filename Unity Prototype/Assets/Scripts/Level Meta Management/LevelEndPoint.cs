﻿using UnityEngine;
using System.Collections;

public class LevelEndPoint : MonoBehaviour {

	public bool introLevel = false;
	private bool hasStarted = false;
	public GameObject logo;
	public GameObject loadPrompt;
	private Utility_ManageGUIText gText;
	public GameObject fadeObject;
	private GameObject fadeObj;
	public GameObject menu;
	private GameObject menuObj;

	public GameObject loadPro;
	public GameObject newLogo;
	public GameObject menuManager;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(hasStarted && fadeObj.GetComponent<Effect_AlphaFade>().curAlpha >= 1 && Mathf.Sign(fadeObj.GetComponent<Effect_AlphaFade>().curAlpha) != -1){
			menuObj = (GameObject)Instantiate(menu, Camera.main.transform.position+new Vector3(0f, 0f, 10f), transform.rotation);
			fadeObj.GetComponent<Effect_AlphaFade>().decSpeed*= -1;
		}else if(hasStarted && fadeObj.GetComponent<Effect_AlphaFade>().curAlpha <= 0 && menuObj != null){
			gText.TextureTweenToGoal(1f);
			Debug.Log("Occurred");
			loadPro = (GameObject)Instantiate(loadPrompt, transform.position, transform.rotation);
			Destroy(fadeObj);
		}
	}

	void OnTriggerStay2D(Collider2D other){
		if(other.tag == "Player")
		{
			SinglePlayerCamera playerCam = Camera.main.GetComponent<SinglePlayerCamera>();
			if(playerCam.canDynamicallyResize && !introLevel)playerCam.ChangeCameraSize(playerCam.minCameraSize, iTween.EaseType.easeOutBounce, 0.5f);
			playerCam.canDynamicallyResize = false;
			playerCam.isResetting = true;		

			RunnerMovement playerMovement = other.GetComponent<RunnerMovement>();
			playerMovement.canAccelerate = false;
			other.rigidbody2D.velocity = Vector2.zero;

			if(!introLevel){
				GameObject.Find("LevelTracker").GetComponent<LevelScoreTracker>().EndLevel();
			}else{
				if(!hasStarted){
					newLogo = (GameObject)Instantiate(logo, Vector3.zero, transform.rotation);
					gText = newLogo.GetComponent<Utility_ManageGUIText>();
					StartGameIntro();
					hasStarted = true;
					//StartCoroutine(IntroDelay(1f));
					Instantiate(menuManager, transform.position, transform.rotation);
					Destroy(GameObject.Find("LevelTracker"));

				}
			}
		}
	}

	IEnumerator IntroDelay(float period){
		yield return new WaitForSeconds(period);
		StartGameIntro();
	}

	IEnumerator LogoDelay(float period){
		yield return new WaitForSeconds(period);

	}

	void StartGameIntro(){
		//Vector3 goalVec = transform.position+ new Vector3(0f, 30f, 0f);
		//goalVec.z = -10f;
		gText.SetupPosition(0f, 0f);
		fadeObj = (GameObject)Instantiate(fadeObject, Camera.main.transform.position+new Vector3(0f, 0f, 10f), transform.rotation);
	}
}
