﻿using UnityEngine;
using System.Collections;

public class LevelScoreTracker : MonoBehaviour {

	/*Actual Score Stuff*/
	public float curScore;
	public float curTime;
	public float curCollection;
	public string collectablesTag;
	private float totalCollectedPossible;
	public bool makeItRain;
	public float coinDropInterval;
	private float curDropCount;
	private bool isBreaking = false;
	public GameObject coinDropObject;

	/*Display Variables*/
	private GUIText displayMesh;
	private float minutes;
	private float seconds;
 	public bool isTicking = false;	

	public bool collectionBased = false;
	public bool timeBasedOnly = false;

	public Texture scoreText;
	public Texture timeText;
	public Texture collectionText;

	public bool guiSizeDebug = false;
	public float guiTextSizeMod;
	public float guiTextSizeModTimeOnly;

	public float divLeft;
	public float divTop;
	public float divWidth;
	public float divHeight;

	//Display Object
	public GameObject timeDisplayObject;
	public GameObject possibleCollectionDisplayObject;
	public GameObject endLevelPromptObject;

	//Tweening variables
	private float newWidth;
	private float newHeightText;
	private float newHeight;
	private int newFontSize;

	public bool getActualSceneName;
	public string levelDescription;
	public GameObject describer;


	public bool levelHasEnded = false;
	private bool textStartedExpanding =false;
	private bool canProgress = false;
	public float progressDelay;

	GameObject endLevelPrompt;

	// Use this for initialization
	void Start () {
		//Display Stuff
		displayMesh = GetComponent<GUIText>();	
		transform.position = Vector3.zero;
		if(getActualSceneName) levelDescription = Application.loadedLevelName;

		if(collectionBased){
			GetComponent<GUITexture>().texture = collectionText;
			guiText.pixelOffset = new Vector2(Screen.width/2+(Screen.width/16), Screen.height/12);
			guiText.fontSize = Mathf.RoundToInt(Screen.height * guiTextSizeModTimeOnly);

		}else if(timeBasedOnly){
			GetComponent<GUITexture>().texture = timeText;
			guiText.pixelOffset = new Vector2(Screen.width/2+(Screen.width/18), Screen.height/5);
			guiText.fontSize = Mathf.RoundToInt(Screen.height * guiTextSizeModTimeOnly);
		}else{
			GetComponent<GUITexture>().texture = scoreText;
			guiText.pixelOffset = new Vector2(Screen.width/2, Screen.height-(Screen.height/14));
			guiText.fontSize = Mathf.RoundToInt(Screen.height * guiTextSizeMod);
		}

		guiTexture.pixelInset = new Rect(Screen.width/divLeft, Screen.height/divTop, Screen.width/divWidth, Screen.height/divHeight);
		// matrix = Matrix4x4.TRS (Vector3.zero, Quaternion.identity, new Vector3(Screen.width/virtualWidth, Screen.height/virtualHeight, 1.0f));
		describer.guiText.text = levelDescription;

		//Score setup
		totalCollectedPossible = GameObject.FindGameObjectsWithTag(collectablesTag).Length;

	}
	
	// Update is called once per frame
	void Update () {

		DebugStuff();

		if(levelHasEnded){
			if(collectionBased) newWidth = Screen.width/divLeft; 
			guiTexture.pixelInset = new Rect(newWidth, newHeightText, Screen.width/divWidth, Screen.height/divHeight);
			guiText.pixelOffset = new Vector2(Screen.width/2+(Screen.width/18), newHeight);

			if(makeItRain && collectionBased && curDropCount < curCollection && !isBreaking){
				StartCoroutine(CoinDropPause(coinDropInterval));
				isBreaking = true;
			}

			if(canProgress && Input.GetKeyUp(KeyCode.Space) ){
				Debug.Log("Levels: " + Application.levelCount);
				Debug.Log("CurLevel: " + (Application.loadedLevel+1));

				if(Application.levelCount > Application.loadedLevel+1){
					Application.LoadLevel(Application.loadedLevel+1);
				}else{
					endLevelPrompt.guiText.text = "No more levels!";
					endLevelPrompt.guiText.material.color = Color.red;
				}
			}
		}else{
			if(isTicking)curTime += Time.deltaTime;
			minutes = Mathf.FloorToInt(curTime / 60F);
			seconds = Mathf.FloorToInt(curTime - minutes * 60);

			if(collectionBased){
				displayMesh.text = ""+curCollection;
			}else if(timeBasedOnly){
				displayMesh.text = string.Format("{0:0}:{1:00}", minutes, seconds);
			}else{
				displayMesh.text = ""+curScore;
			}
		}

		if(guiText.pixelOffset.y == newHeight && !textStartedExpanding && GetComponent<iTween>() == null){
			//textStartedExpanding = true;
			//ChangeTextPromp(Screen.height*(guiTextSizeModTimeOnly+0.4f), iTween.EaseType.easeInBounce, 3f);
			//guiText.text = "Space to Continue!";
		}

		//guiText.fontSize = newFontSize;

	}

		public void EndLevel(){
		if(!levelHasEnded){
			guiSizeDebug = false;
			isTicking = false;
			levelHasEnded = true;
			if(collectionBased){
				ChangeTexturePosTop(Screen.height/0.8f, iTween.EaseType.easeOutBounce, 1f);

				GameObject timeDisplay = (GameObject) Instantiate(timeDisplayObject, Vector3.zero, transform.rotation);
				timeDisplay.guiText.text = string.Format("{0:0}:{1:00}", minutes, seconds);
				timeDisplay.GetComponent<Utility_ManageGUIText>().SetupPosition(-Screen.width/10, Screen.height/1.1f);
				timeDisplay.GetComponent<Utility_ManageGUIText>().ChangeTextWidth(Screen.width/2-Screen.width/3, iTween.EaseType.easeOutExpo, 1f);

				GameObject collectionDisplay = (GameObject) Instantiate(possibleCollectionDisplayObject, Vector3.zero, transform.rotation);
				collectionDisplay.guiText.text = "/" + totalCollectedPossible;
				collectionDisplay.GetComponent<Utility_ManageGUIText>().SetupPosition(Screen.width+Screen.width/10, Screen.height/1.2f);
				collectionDisplay.GetComponent<Utility_ManageGUIText>().ChangeTextWidth(Screen.width/2+(Screen.width/6), iTween.EaseType.easeOutExpo, 1f);

				endLevelPrompt = (GameObject) Instantiate(endLevelPromptObject, Vector3.zero, transform.rotation);
				endLevelPrompt.guiText.text= "Space to Continue";
				endLevelPrompt.GetComponent<Utility_ManageGUIText>().SetupPosition(Screen.width/2, Screen.height/12);
				endLevelPrompt.GetComponent<Utility_ManageGUIText>().ChangeTextHeight(Screen.height/8, iTween.EaseType.easeOutExpo, 1f);
				//	.GetComponent<Utility_ManageGUIText>().ChangeTextHeight(Screen.height/1.1f, iTween.EaseType.easeOutExpo, 1f);
			}else{
				ChangeTexturePos(Screen.width/-2, iTween.EaseType.easeOutBounce, 3f);
			}
			if(timeBasedOnly)ChangeTextPos(Screen.height/1.1f, iTween.EaseType.easeOutBounce, 1f);
			StartCoroutine(AllowProgressionDelay(progressDelay));
		}
	}

	#region Juicing

	//Textures
	public void ChangeTexturePos(float newLeftGoal, iTween.EaseType easeType, float period){
		iTween.ValueTo( gameObject, iTween.Hash(
			"from", Screen.width/divLeft,
			"to", newLeftGoal,
			"time", period,
			"onupdatetarget", gameObject,
			"onupdate", "tweenOnUpdateCallBack",
			"easetype", easeType
			)
		               );
	}
	
	void tweenOnUpdateCallBack( int newValue )
	{
		newWidth = newValue;
		//camera.orthographicSize = currentCameraSize;
	}

	public void ChangeTexturePosTop(float newTopGoal, iTween.EaseType easeType, float period){
		iTween.ValueTo( gameObject, iTween.Hash(
			"from", Screen.height/divTop,
			"to", newTopGoal,
			"time", period,
			"onupdatetarget", gameObject,
			"onupdate", "tweenOnUpdateCallBackTop",
			"easetype", easeType
			)
		               );
	}
	
	void tweenOnUpdateCallBackTop( int newValue )
	{
		newHeightText = newValue;
		//camera.orthographicSize = currentCameraSize;
	}

	//Text

	public void ChangeTextPos(float newTopGoal, iTween.EaseType easeType, float period){
		iTween.ValueTo( gameObject, iTween.Hash(
			"from", Screen.height/divTop,
			"to", newTopGoal,
			"time", period,
			"onupdatetarget", gameObject,
			"onupdate", "tweenOnUpdateCallBackText",
			"easetype", easeType
			)
		               );
	}
	
	void tweenOnUpdateCallBackText( int newValue )
	{
		newHeight = newValue;
		//camera.orthographicSize = currentCameraSize;
	}

	public void ChangeTextSize(float newTextSize, iTween.EaseType easeType, float period){
		iTween.ValueTo( gameObject, iTween.Hash(
			"from", Screen.height/5,
			"to", newTextSize,
			"time", period,
			"onupdatetarget", gameObject,
			"onupdate", "tweenOnUpdateCallBackTextSize",
			"easetype", easeType
			)
		               );
	}
	
	void tweenOnUpdateCallBackTextSize( int newValue )
	{
		newFontSize = newValue;
		//camera.orthographicSize = currentCameraSize;
	}
	#endregion Juicing

	#region Timers
	IEnumerator CoinDropPause(float period)
	{
		yield return new WaitForSeconds(period);
		Instantiate(coinDropObject, Camera.main.ScreenToWorldPoint( new Vector3(Screen.width/2, Screen.height+Screen.width/8, 10f)), transform.rotation);
		isBreaking = false;
		curDropCount++;
	}

	IEnumerator AllowProgressionDelay(float period)
	{
		yield return new WaitForSeconds(period);
		canProgress = true;
	}

	#endregion Timers

	void DebugStuff(){
		if(guiSizeDebug){
			if(collectionText){
				guiTexture.texture = collectionText;
			}else
			if(timeBasedOnly){
				guiTexture.texture = timeText;

			}else{
				guiTexture.texture = scoreText;
				//guiText.pixelOffset = new Vector2(Screen.width/2, Screen.height-(Screen.height/14));
				//guiText.fontSize = Mathf.RoundToInt(Screen.height * guiTextSizeMod);
			}

			guiText.pixelOffset = new Vector2(Screen.width/2+(Screen.width/18), Screen.height/1.1f);
			//guiText.fontSize = Mathf.RoundToInt(Screen.height * guiTextSizeModTimeOnly);
			
			guiTexture.pixelInset = new Rect(Screen.width/divLeft, Screen.height/divTop, Screen.width/divWidth, Screen.height/divHeight);
		}

		if(Input.GetKeyUp(KeyCode.E))GameObject.Find("Runner Player 1").transform.position = GameObject.Find("Level End Point").transform.position;
	}
}
