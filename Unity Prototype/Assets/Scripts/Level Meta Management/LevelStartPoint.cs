﻿using UnityEngine;
using System.Collections;

public class LevelStartPoint : MonoBehaviour {

	//Time Based
	public bool timeBasedLevelStart;
	public float startCountdownDelay;
	public float startSpeed;
	public float startDelay;
	//Button Based
	public KeyCode buttonToStart;
	public float guiTextSizeMod;
	public GameObject promptObj;

	public GameObject startingObject;
	public string startingObjectName;

	private float currentTime = 0f;

	private bool text3 = false;
	private bool text2 = false;
	private bool text1 = false;

	public GameObject textEffect;
	private bool hasStarted = false;
	private bool levelStarted = false;

	public GameObject paintExplosion;

	public GameObject Foreground;
	public GameObject background;

	//For text countdown
	int pastInt = 0;

	// Use this for initialization
	void Start () {
		if(startingObject == null) startingObject = GameObject.Find(startingObjectName);
		startingObject.transform.position = transform.position;
		Camera.main.transform.position = startingObject.transform.position - new Vector3(0f, 0f, 10f);
		startingObject.GetComponent<RunnerMovement>().canAccelerate = false;
		if(timeBasedLevelStart){
			StartCoroutine(CountdownDelay(startCountdownDelay));
			promptObj.guiText.enabled = false;
		}else{
			SetupPrompt();
		}
	}
	
	// Update is called once per frame
	void Update () {

		if(timeBasedLevelStart){
			if(hasStarted){
				if(currentTime >0){
					currentTime -= Time.deltaTime;
					//Debug.Log("Mod: " + currentTime % 1);

					TextCountdown(currentTime);
					/*if(1 == 0 ){

					}*/

					/*if(currentTime <= 2 && currentTime > 1 && !text2){
						text2 = true;
						GameObject newEffect = (GameObject) Instantiate(textEffect, Camera.main.transform.position, transform.rotation);
						newEffect.GetComponent<Effect_Text>().Setup(Color.white, 1f, "2", false, 0f, true, true, 10, 30);
					}else if(currentTime <= 1 && !text1){
						text1 = true;
						GameObject newEffect = (GameObject) Instantiate(textEffect, Camera.main.transform.position, transform.rotation);
						newEffect.GetComponent<Effect_Text>().Setup(Color.white, 1f, "1", false, 0f, true, true, 10, 30);
					}*/

				}else if(!levelStarted){
					StartLevel();
				}
			}
		}else if(Input.GetKeyUp(buttonToStart) && !hasStarted){
			StartLevel();
		}

		promptObj.guiText.fontSize = Mathf.RoundToInt( Screen.width *guiTextSizeMod);
	}

	#region TimeBased
	void TextCountdown(float valueToCheck){
		int curInt = Mathf.FloorToInt(valueToCheck);

		if( valueToCheck - curInt <= 0.1 && curInt != pastInt){
			pastInt = curInt;
			GameObject newEffect = (GameObject) Instantiate(textEffect, Camera.main.transform.position, transform.rotation);
			newEffect.GetComponent<Effect_Text>().Setup(Color.black, 1f, (curInt != 0) ? ""+curInt : "GO!", false, 0f, true, true, 10, 30);
		}
	}

	void StartLevel()
	{
		RunnerMovement mover = startingObject.GetComponent<RunnerMovement>();
		background.GetComponent<Animator>().SetBool("canOpen", true);
		Foreground.GetComponent<Animator>().SetBool("canOpen", true);

		mover.canAccelerate = true;
		startingObject.rigidbody2D.velocity = new Vector3(startSpeed*startingObject.GetComponent<RunnerMovement>().moveMod, 0f, 0f);
		Debug.Log("Velocity: " + startingObject.rigidbody2D.velocity.x);

		GameObject.Find("LevelTracker").GetComponent<LevelScoreTracker>().isTicking = true;
		levelStarted = true;
		promptObj.guiText.enabled = false;
		Instantiate(paintExplosion, transform.position+new Vector3(10f, 0f, 0f), transform.rotation);
		hasStarted = true;
		//Destroy(gameObject);
	}
	#endregion TimeBased

	#region ButtonBased
	void SetupPrompt(){
		promptObj.guiText.fontSize = Mathf.RoundToInt( Screen.width *guiTextSizeMod);
		promptObj.guiText.text = buttonToStart.ToString() + " To start";
		promptObj.guiText.pixelOffset = new Vector2(Screen.width/2+Screen.width/5, Screen.height/2);
		promptObj.transform.position = Vector3.zero;
	}
	#endregion ButtonBased

	IEnumerator CountdownDelay(float period){
		yield return new WaitForSeconds(period);
		currentTime = startDelay;
		TextCountdown(currentTime);
		hasStarted = true;
	}
}
