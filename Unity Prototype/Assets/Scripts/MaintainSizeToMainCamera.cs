﻿using UnityEngine;
using System.Collections;

public class MaintainSizeToMainCamera : MonoBehaviour {

	private float currentCameraSize;
	private float percSizeOfMain;
	
	// Use this for initialization
	void Start () {
		percSizeOfMain = camera.orthographicSize/Camera.main.orthographicSize;
	}
	
	// Update is called once per frame
	void Update () {
		camera.orthographicSize = Camera.main.orthographicSize *percSizeOfMain;
		DebugControls();
	}

	void DebugControls(){
		if(Input.GetKeyUp(KeyCode.G)){
			percSizeOfMain = camera.orthographicSize/Camera.main.orthographicSize;
		}
	}
}
