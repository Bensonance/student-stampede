﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour {

	public bool provideScore;
	public float scoreGiven;
	public AudioClip[] scorePickupSounds; 
	private AudioClip lastSound;
	public bool canPlayLast;
	public bool randomisePitch;
	public float minPitch;
	public float maxPitch;

	private bool destroySelf = false;

	public bool makeCoinSplosion;
	public GameObject effectCoinSplosion;

	// Use this for initialization
	void Start () {
		tag = "DefaultCollectable";
		GetComponent<SpriteRenderer>().sortingLayerName = "Characters";
		GetComponent<SpriteRenderer>().material.color = Color.red;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "Player"){

			int sel = Mathf.RoundToInt(Random.Range(0f, scorePickupSounds.Length-0.6f));
			AudioClip goalClip = scorePickupSounds[sel];

			if(!canPlayLast){
				while(lastSound == goalClip){
					sel = Mathf.RoundToInt(Random.Range(0f, scorePickupSounds.Length-0.6f));
					goalClip = scorePickupSounds[sel];
				}
			}
			AudioSource.PlayClipAtPoint(goalClip, transform.position, 0.2f);
			//if(randomisePitch)newSound.pitch = Random.Range(minPitch, maxPitch); 
			destroySelf = true;
			Destroy(gameObject);
		}
	}

	void OnDestroy(){
		if(provideScore && destroySelf){
			GameObject.Find("LevelTracker").GetComponent<LevelScoreTracker>().curScore += scoreGiven;
			GameObject.Find("LevelTracker").GetComponent<LevelScoreTracker>().curCollection++;
			Camera.main.gameObject.GetComponent<SinglePlayerCamera>().ShakeScreen(0.1f, 0.1f);
			if(makeCoinSplosion){
				GameObject coinExplosion = (GameObject) Instantiate(effectCoinSplosion, transform.position, transform.rotation);
				coinExplosion.rigidbody2D.velocity = new Vector2(GameObject.Find("Runner Player 1").rigidbody2D.velocity.x+20f, 10f);
				coinExplosion.particleSystem.renderer.sortingLayerName = "Effects";
			}
		}
	}
}
