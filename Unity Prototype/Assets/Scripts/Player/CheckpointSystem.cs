﻿using UnityEngine;
using System.Collections;

public class CheckpointSystem : MonoBehaviour {

	public GameObject currentCheckpoint;
	private GameObject playerObject;
	public float resetPlayerLeeway;

	public bool isResetting = false;
	// Use this for initialization
	void Start () {
		playerObject = gameObject;
		currentCheckpoint = GameObject.Find("Level Start Point");
		StartCoroutine(ResetLeeway(resetPlayerLeeway));
	}
	
	// Update is called once per frame
	void Update () {
		if(isResetting){
			if(Vector3.Distance(playerObject.transform.position, currentCheckpoint.transform.position) <= 0.5f){
				RunnerMovement playerMove =playerObject.GetComponent<RunnerMovement>();
				playerMove.state.isResetting =false;
				playerMove.canAccelerate = true;
				playerMove.state.recentlyReset = true;
				isResetting = false;
				StartCoroutine(ResetLeeway(resetPlayerLeeway));
				Debug.Log("Occurred");
			}
		}
	}

	public void ResetPlayer(){
		iTween.MoveTo(playerObject, currentCheckpoint.transform.position, 3f);	
		isResetting = true;
		Debug.Log("Resetting Player");
	}

	IEnumerator ResetLeeway(float period){
		yield return new WaitForSeconds(period);
		playerObject.GetComponent<RunnerMovement>().state.recentlyReset = false;
	}
}
