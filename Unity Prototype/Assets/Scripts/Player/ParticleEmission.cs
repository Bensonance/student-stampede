﻿using UnityEngine;
using System.Collections;

public class ParticleEmission : MonoBehaviour {

	private ParticleSystem playerSys;
	private StateTracker playerState;

	public float minEmissionRate;
	public float maxEmissionRate;

	public float minPartXSpeed;
	public float maxPartXSpeed;

	public float minPartYGrav;
	public float maxPartYGrav;
	//Debugging
	public bool debugParticles;

	// Use this for initialization
	void Start () {
		playerSys= GetComponent<ParticleSystem>();
		playerState = transform.parent.gameObject.GetComponent<StateTracker>();
		playerSys.Play();
	}
	
	// Update is called once per frame
	void Update () {
		if(playerState.onGround && playerState.moving){
			if(!playerSys.isPlaying)playerSys.Play();
		
			//playerSys.particleEmitter.ve
			if(debugParticles)Debug.Log("Playing");
		}else{
			playerSys.Stop();
			if(debugParticles)Debug.Log("Stopping");
		}

		playerSys.emissionRate = CalculateRatioToPlayer(minEmissionRate, maxEmissionRate, false);
	}

	void LateUpdate(){
		SetParticleSpeed();
	}	

	public float CalculateRatioToPlayer(float minValue, float maxValue, bool inverse){
		float percSpeed = transform.parent.gameObject.rigidbody2D.velocity.x / transform.parent.gameObject.GetComponent<RunnerMovement>().maxMoveSpeed;
		percSpeed = inverse ? 1-percSpeed : percSpeed;
		//if(inverse) Debug.Log("Reversed speed: " + percSpeed);
		percSpeed = Mathf.Clamp(percSpeed, 0, 1);
		float ratio = percSpeed * maxValue;
		ratio = Mathf.Clamp(ratio, minValue, maxValue);
		return ratio;
	}

	void SetParticleSpeed(){
		ParticleSystem.Particle[] p = new ParticleSystem.Particle[playerSys.particleCount+1];
		int l = particleSystem.GetParticles(p);

		//Debug.Log("Speed: " + CalculateRatioToPlayer(minPartYGrav, maxPartYGrav, true));

		playerSys.gravityModifier = CalculateRatioToPlayer(minPartYGrav, maxPartYGrav, true);

		
		int i = 0;
		while (i < l) {
			p[i].velocity = new Vector3(0, 0, 
			                            (p[i].lifetime / p[i].startLifetime) * CalculateRatioToPlayer(minPartXSpeed, maxPartXSpeed, false));
			i++;
			if(debugParticles){
				Debug.Log("Lifetime perc: " + p[i].lifetime / p[i].startLifetime);
				Debug.Log("Speed: " + (p[i].lifetime / p[i].startLifetime) * CalculateRatioToPlayer(minPartXSpeed, maxPartXSpeed, false));		
			}
		}
		
		particleSystem.SetParticles(p, l);  
	}
}
