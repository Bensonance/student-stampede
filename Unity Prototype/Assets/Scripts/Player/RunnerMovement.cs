﻿using UnityEngine;
using System.Collections;

//[RequireComponent (typeof (Rigidbody2D))]
public class RunnerMovement : MonoBehaviour {

	#region Variables

	/*Movement*/
	public float minMoveSpeed;
	public float maxMoveSpeed;
	public float startMaxMoveSpeed;
	private bool maxSpeedIncreased = false;
	public float accMoveForce;
	public float accMoveForceInAir;
	public float accMoveForceUpSlope;
	public float accMoveForceDownSlope;
	public float accFlatSlopeLeeway; //Variation in angle that will still interpret as a flat surface
	private Vector2 curSpeed;
	public bool canAccelerate = false;
	public string currentFloorTag;
	public int moveMod; //1 = right ; -1 = left;
	private float xMoveLand;

	/*Jumping*/
	public KeyCode jumpKey;
	public float jumpSpeed;
	public bool canJump = true;
	public bool jumpAllowed;
		//Jump Buffering
		private bool startedBuffer; 
		public float bufferPeriod;
		//Jump Pressure Senisit
		public float maxJumpDist;
		private bool hasJumped = false;
		private float jumpYStart = 0f;
	private float landSpeed;

	/*Diving*/
	public bool diveAllowed = false;
	 public bool jumpReleased = false;
	public float diveAccSpeed;
	public float diveMaxSpeed;
	public GameObject effectBlur;
	private GameObject blurEffect;


	/*Wall Sliding*/
	public float wallSlideGravityScale;
	public float wallSlideJumpXPushOffVelocityBase;
	private int wallJumpChain =1;
	public float wallJumpJumpYBoost;
	public int maxJumpChain;
	public float resetJumpChainDelay;
	private bool isTiming = false;

	/*Collisions*/
	public float floorRayLength;
	public LayerMask floorLayer;
	private Vector2 desiredDirection;

	public float wallRayLength;
	public LayerMask wallLayer;
	public float wallSlideMargin;

	public float terminalFallSpeed;


	//Fail Condition
	public bool canDie = false;
	public float failSpeed = 2f;
	private CheckpointSystem checkSys;

	/*Physics*/
	public float gravityScale = 2;
	public StateTracker state;

	/*Testing*/
	public bool flatSurfaceTesting;
	public bool debugSlopeAcceleration;
	public bool debugJumping = true;
	public bool debugCollisions;

	/*Sounds*/
	private SoundTracker playerSound;

	/*Iterative Features*/
	public bool forcingJumpAnticipation = false;
	private int jumpState;
	public bool debugMovement;

	public float jumpBufferDownRayLength;

	/*Internal States*/

	#endregion Variables
	// Use this for initialization
	void Start () {
		state = GetComponent<StateTracker>();
		checkSys = GetComponent<CheckpointSystem>();
		playerSound = GetComponent<SoundTracker>();
		jumpState = Animator.StringToHash("Base.Jump Blend Tree");
		Debug.Log("JumpState: " + jumpState);	
		startMaxMoveSpeed = maxMoveSpeed;
		//ChangeColor();
	}
	
	// Update is called once per frame
	void Update () {

		if(jumpAllowed)Jump();
		if(diveAllowed)Diving();
		Collisions();
		DebugControls();
		//Debug.Log("Velocity: " + rigidbody2D.velocity.magnitude);
	}

	void FixedUpdate(){
		WallSliding();
		Movement();

		if(canDie && rigidbody2D.velocity.x <= failSpeed && !state.isResetting && !state.recentlyReset){
			state.isResetting =true;
			canAccelerate = false;
			rigidbody2D.velocity = Vector2.zero;
			checkSys.ResetPlayer();
		}


		//if(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).tagHash == jumpState)Debug.Log("Uhhhhhm");
		//Debug.Log("Int: " + GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).tagHash);
		//Debug.Log("Jump: " + GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).nameHash);
	}

	#region MovementFunctions

	void Movement(){
		if(canAccelerate && !state.isResetting){
			desiredDirection = CalculateIncline();

			Vector3 moveDirection = new Vector3(desiredDirection.x, desiredDirection.y, 0f);
				
			rigidbody2D.velocity += CheckAccelerationType()*desiredDirection*Time.deltaTime;
			//if(!state.isJumping)rigidbody2D.velocity = rigidbody.velocity.magnitude * moveDirection;

			float curXSpeed = rigidbody2D.velocity.x;
			curXSpeed = Mathf.Clamp(curXSpeed, -maxMoveSpeed, maxMoveSpeed);
			rigidbody2D.velocity = new Vector2(curXSpeed, rigidbody2D.velocity.y);
		}

		if(state.onGround && state.moving && !state.isJumping){
			playerSound.PlayRunSound(rigidbody2D.velocity.x);
		}else if(audio.clip.name.ToString().Contains("run")){
			audio.Stop();
		}

		if(!state.onGround && maxMoveSpeed == startMaxMoveSpeed && canAccelerate && !state.isResetting){
			rigidbody2D.velocity += new Vector2(accMoveForceInAir, 0f);
		}

		if(debugMovement) Debug.Log("Velocity: " + rigidbody2D.velocity);
	}

	void Jump(){
		if(canJump && (state.moving || state.isWallSliding) && Input.GetKeyDown(jumpKey)){//If can jump and pressed jump button
			//if(!state.onGround)Debug.Break();
			if(!forcingJumpAnticipation){
				Vector3 curSpeed = rigidbody2D.velocity;
				curSpeed.y = jumpSpeed;
				if(state.isWallSliding){//Jumping when wall slidng
					curSpeed.x = (wallSlideJumpXPushOffVelocityBase *wallJumpChain )*-(moveMod);
					moveMod*=-1;
					wallJumpChain++;
					if(!isTiming){
						StartCoroutine(ResetWallJumpChain(resetJumpChainDelay));
						isTiming = true;
					}
					curSpeed.y += wallJumpJumpYBoost*wallJumpChain;
				}
				rigidbody2D.velocity = curSpeed;
			}
			jumpReleased = false;
			canJump = false;
			StopCoroutine("JumpBufferPeriod"); //Stops jump buffer to avoid being alowed to jump twice
			hasJumped = true;
			jumpYStart = transform.position.y;	
			state.isJumping = true;
			playerSound.PlayJumpSound();
		}else if(hasJumped && Input.GetKey(jumpKey)){//Pressure sensitve jump
///			Debug.Log("Int hash: " + jumpState + " Actual hash: " + GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).nameHash);
		
			if(!forcingJumpAnticipation || GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Jump Blend Tree")){
				Vector3 curSpeed = rigidbody2D.velocity;
				curSpeed.y = jumpSpeed;
				if(state.isWallSliding)curSpeed.y += wallJumpJumpYBoost*wallJumpChain;
				rigidbody2D.velocity = curSpeed;
			}
			state.isJumping = true;
			if(transform.position.y >= jumpYStart+maxJumpDist){
				hasJumped = false;
				state.isJumping = false;
			}
		}else if(Input.GetKeyUp(jumpKey)){
			if(forcingJumpAnticipation && state.onGround){
				Vector3 curSpeed = rigidbody2D.velocity;
				curSpeed.y = jumpSpeed;
				if(state.isWallSliding)curSpeed.y += wallJumpJumpYBoost*wallJumpChain;
				rigidbody2D.velocity = curSpeed;
			}
			hasJumped = false;
			canJump = false;
			state.isJumping = false;
		}

		//if(rigidbody2D.velocity.y <=0) hasJumped = false;

		if(debugJumping)Debug.Log("VSpeed: "+ rigidbody2D.velocity.y);
	}

	void WallSliding(){
		if(state.isWallSliding)
		{
			rigidbody2D.gravityScale = wallSlideGravityScale;
			if(rigidbody2D.velocity.y >0) rigidbody2D.velocity = Vector2.zero;
			canJump = true;
			StopAllCoroutines();
			isTiming = false;
		}
	}

	void Diving()
	{
		if(Input.GetKeyUp(jumpKey))jumpReleased = true;

		if(jumpReleased && !state.onGround && Input.GetKey(jumpKey) && !canJump)
		{
			state.isDiving = true;
			rigidbody2D.velocity += new Vector2(0f, -diveAccSpeed);
			float curFallSpeed = rigidbody2D.velocity.y;
			curFallSpeed = Mathf.Clamp(curFallSpeed, -diveMaxSpeed, diveMaxSpeed);
			//renderer.material.color = Color.red;

			if(blurEffect == null){
				blurEffect =(GameObject) GameObject.Instantiate(effectBlur);
			}
		}else
		{
			if(blurEffect != null){
				Destroy(blurEffect);
				GameObject[] blurs = GameObject.FindGameObjectsWithTag("Blurs");
				foreach(GameObject f in blurs){
					Destroy(f);
				}
			}
			state.isDiving = false;
			//renderer.material.color = Color.white;
		}
	}
	#endregion MovementFunctions

	#region Collisions
	void Collisions(){

		//Vertical Collisions

		Vector3 orgin = transform.position;
		//orgin.y -= collider2D.bounds.extents.y;
		RaycastHit2D floorTest = Physics2D.Raycast(orgin, Vector3.down,  state.onGround? floorRayLength : Mathf.Max(rigidbody2D.velocity.y *Time.deltaTime+floorRayLength*2, floorRayLength), floorLayer); //Check for collisions in the floor layer

		if(floorTest.collider != null){//I.E. if there is a collider

			if(!state.onGround){ //If is landing
				if(floorTest.transform.tag.Contains("Slope") && rigidbody2D.velocity.y>0){
					//Camera.main.gameObject.GetComponent<SinglePlayerCamera>().canDynamicallyResize = false;
					//StartCoroutine(Camera.main.gameObject.GetComponent<SinglePlayerCamera>().DynamicCameraDelay(1f));
				}
				GetComponent<Animator>().SetFloat("vSpeed", rigidbody2D.velocity.y);
				landSpeed = rigidbody2D.velocity.y;
				xMoveLand = rigidbody2D.velocity.x+20f;

				GetComponent<Animator>().SetFloat("landSpeed", landSpeed);
				state.onGround = true;
				if(!floorTest.transform.tag.Contains("Slope") ){
					rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0f);
					Debug.Log("Stopped vert speed:");
				}else{

					//rigidbody2D.velocity = new Vector2(xMoveLand, rigidbody2D.velocity.y);
				}

				//Debug.Log("Distance: " + floorTest.distance);
				float dist = floorTest.distance; 

				if(!state.isJumping)transform.Translate(Vector3.down*(dist));
				Debug.Log("Occurring");
				state.isDiving = false;
				jumpReleased = false;

			

			}else if(floorTest.distance >0.5f && !state.isJumping){
				transform.Translate(Vector3.down*(floorTest.distance));
				//curSpeed.y = 0f;
			}
			currentFloorTag = floorTest.collider.tag;

			canJump = true;
			startedBuffer = false;
			
		}else{
			if(!startedBuffer){
				startedBuffer = true;
				StartCoroutine(JumpBufferPeriod(bufferPeriod));//Start jump buffer period
			}
			state.onGround = false;
		}

		/*Jump Buffer test*/
		RaycastHit2D bufferTest = Physics2D.Raycast(orgin, Vector3.down,  state.onGround? jumpBufferDownRayLength : Mathf.Max((rigidbody2D.velocity.y +jumpBufferDownRayLength) *Time.deltaTime, jumpBufferDownRayLength), floorLayer); //Check for collisions in the floor layer

		if(bufferTest.collider != null){
			canJump = true;
			//if(!state.onGround && !hasJumped)Debug.Break();
		}

		//Gravity
		if(state.onGround){
			rigidbody2D.gravityScale = 0;
		}else{
			if(state.canReset && !state.isWallSliding)rigidbody2D.gravityScale = gravityScale;
		}

		//Terminal Velocity
		/*float curYSpeed = rigidbody2D.velocity.y;
		curYSpeed = Mathf.Clamp(curYSpeed, -terminalFallSpeed, jumpSpeed);
		rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, curYSpeed);*/



		//Horizontal Collisions
		Vector3 originHorzontal = transform.position+new Vector3(collider2D.bounds.extents.x*moveMod, 0f, 0f);
		//orgin.y -= collider2D.bounds.extents.y;
		RaycastHit2D wallTest = Physics2D.Raycast(originHorzontal, Vector3.right*moveMod,  Mathf.Max(wallRayLength, Mathf.Abs(rigidbody2D.velocity.x *Time.deltaTime)+wallRayLength), wallLayer); //Check for collisions in the floor layer
		if(wallTest.collider != null && !state.onGround){
			if(!state.isWallSliding){
				Debug.Log("Hit wall");
				canAccelerate = false;
				rigidbody2D.velocity = Vector2.zero;
				transform.Translate((Vector3.right *moveMod )*(wallTest.distance+wallSlideMargin) );
				state.isWallSliding = true;
				StopCoroutine("ResetWallJumpChain");
				isTiming = false;
			}
		}else if(state.isWallSliding){
			state.isWallSliding = false;
			canAccelerate = true;
		}

		if(debugCollisions){
			Debug.DrawRay(orgin, Vector3.down*(state.onGround? floorRayLength :-rigidbody2D.velocity.y *Time.deltaTime), Color.red);
			//Debug.DrawRay(orgin, Vector3.down*(state.onGround? jumpBufferDownRayLength :Mathf.Max((rigidbody2D.velocity.y +jumpBufferDownRayLength) *Time.deltaTime, jumpBufferDownRayLength )), Color.black);
			Debug.DrawRay(originHorzontal, Vector3.right*moveMod*(Mathf.Max(wallRayLength, Mathf.Abs(rigidbody2D.velocity.x *Time.deltaTime)+wallRayLength)), Color.yellow);
		}
 	}

	float CheckAccelerationType(){
		if(currentFloorTag == "UpSlope"){
			if(debugSlopeAcceleration)Debug.Log("Up slope acc");
			return accMoveForceUpSlope*moveMod;
		}else if(currentFloorTag == "DownSlope"){
			if(debugSlopeAcceleration)Debug.Log("Down slope acc");
			return accMoveForceDownSlope*moveMod;
		}else{
			if(debugSlopeAcceleration)Debug.Log("Straight plaform acc");
			return accMoveForce*moveMod;
		}
	}
	#endregion Collisions

	#region Timers
	IEnumerator JumpBufferPeriod(float period){//Period after player falls off floor, that they can still jump for.
		yield return new WaitForSeconds(period);
		canJump = false;
	}

	IEnumerator ResetWallJumpChain(float period){
		yield return new WaitForSeconds(period);
		if(!state.isWallSliding)wallJumpChain = 1;
		isTiming = false;
	}

	IEnumerator ResetMaxMoveSpeed(float period){
		yield return new WaitForSeconds(period);
		maxMoveSpeed = startMaxMoveSpeed;
		maxSpeedIncreased = false;
	}

	IEnumerator JumpAllowedDelay(float period){
		yield return new WaitForSeconds(period);
		jumpAllowed = true;
	}
	#endregion Timers

	#region Utility
	Vector2 CalculateIncline(){
		float angle = CalculateAngle();
		return new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
	}
	
	float CalculateAngle(){
		Vector3 origin = transform.position;
		float a=transform.rotation.eulerAngles.z*Mathf.Deg2Rad;
		//origin.y += collider2D.bounds.extents.y;
		//origin.x += Mathf.Sign(transform.localScale.x)*collider2D.bounds.extents.x;
		RaycastHit2D inclineCheck = Physics2D.Raycast(origin, Vector3.down, floorRayLength, floorLayer);
		//if(debugCollisions)Debug.DrawRay(origin, Vector3.down, Color.green);
		
		Vector2 floorNormal = inclineCheck.normal;
		
		return Mathf.Atan2(floorNormal.y, floorNormal.x)-Mathf.PI/2;
	}

	public void ChangeMoveModBasedOnSpeed(){
		if(rigidbody2D.velocity.x > 0){
			moveMod = 1;
		}else{
			moveMod = -1;
		}
	}

	public void IncreaseMaxSpeed(float newMaxSpeed, float period){
		if(!maxSpeedIncreased){
			maxMoveSpeed = newMaxSpeed;
			//maxSpeedIncreased = true;
			StopCoroutine("ResetMaxMoveSpeed");
			StartCoroutine(ResetMaxMoveSpeed(period));
		}
	}

	public void DisallowJumpingPeriod(float period){
		jumpAllowed = false;
		StartCoroutine(JumpAllowedDelay(period));
	}

	public void ChangeColor(string text){
		renderer.material.color = Color.red;
	}

	void DebugControls(){
		if(Input.GetKeyUp(KeyCode.Return)) Application.LoadLevel(Application.loadedLevel);
		if(Input.GetKeyUp(KeyCode.Escape))Application.Quit();
		if(Input.GetKeyUp(KeyCode.E))transform.position = GameObject.Find("Level End Point").transform.position;
		if(Input.GetKey(KeyCode.LeftShift)){
			Application.LoadLevel(Application.loadedLevel-1);
		}

		if(Input.GetKeyUp(KeyCode.Tab)){
			Application.LoadLevel(Application.loadedLevel+1);
		}
	
	}
	#endregion Utility
}
