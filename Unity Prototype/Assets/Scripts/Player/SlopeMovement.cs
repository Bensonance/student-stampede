using UnityEngine;
using System.Collections;

public class SlopeMovement : MonoBehaviour {

	public float flatAcceleration = 4f;
	public float upSlopeAcceleration = 2f;
	public float downSlopeAcceleration = 6f;
	float maxSpeed = 150f;
	float jumpSpeed = 100f;
	float gravity = 6f;
	float maxFall = 200f;
	float jump = 200f;
	string slope = "";

	public bool canAccelerate = true;

	public float angleLeeway;
	public LayerMask whatIsFloor;

	Rect box;

	[HideInInspector] public Vector2 velocity;

	bool grounded = false;
	bool falling = false;

	int horizontalRays = 6;
	int verticalRays = 4;
	int margin = 2;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		box = new Rect(	
		               collider2D.bounds.min.x,
		               collider2D.bounds.min.y,
		               collider2D.bounds.size.x,
		               collider2D.bounds.size.y
		               );
		if(!grounded) velocity = new Vector2(velocity.x, Mathf.Max(velocity.y - gravity, -maxFall));


		LateralMovement();
		VerticalMovement();
	}

	void LateUpdate(){
		transform.Translate(velocity *Time.deltaTime);
	}

	void VerticalMovement(){


		if(velocity.y <0)falling = true;
		
		if(grounded || falling){
			Vector3 startPoint = new Vector2(box.xMin + margin, box.center.y);
			Vector3 endPoint = new Vector2(box.xMax - margin, box.center.y);
			RaycastHit2D[] hitInfos =  new RaycastHit2D[verticalRays];
			
			float distance = box.height/2 + (grounded ? margin : Mathf.Abs(velocity.y * Time.deltaTime));
			
			float smallestFraction = Mathf.Infinity;
			int indexUsed = 0;
			
			bool connected = false;
			
			for(int i = 0; i < verticalRays; i++){
				float lerpAmount = (float)i/ (float) (verticalRays -1);
				Vector2 origin = Vector2.Lerp(startPoint, endPoint, lerpAmount);
				
				hitInfos[i] = Physics2D.Raycast(origin, -Vector2.up, distance, whatIsFloor);

				if(hitInfos[i].fraction >0 ){
					connected = true;
					if(hitInfos[i].fraction < smallestFraction){
						indexUsed = i;
						smallestFraction = hitInfos[i].fraction;
					}
				}
				if(hitInfos[i].transform != null)slope = hitInfos[i].transform.tag;
			}
			
			if(connected){
				grounded = true;
				falling = false;
				transform.Translate(Vector3.down *(hitInfos[indexUsed].fraction *distance - box.height/2));
				velocity = new Vector2(velocity.x, 0f);
			}
			else{
				grounded = false;
				slope = "";
			}

			if(Input.GetKey(KeyCode.Space) && grounded){
				velocity = new Vector2(velocity.x, jumpSpeed);
			}
		}else{

		}
	}

	void LateralMovement(){
		float horizontalAxis = Input.GetAxisRaw("Horizontal");
		float newVelocityX = velocity.x;

		if(canAccelerate && grounded){
			if(slope == "DownSlope"){
				newVelocityX += downSlopeAcceleration;
				Debug.Log("Down Slope");
			}else if(slope == "UpSlope"){
				newVelocityX += upSlopeAcceleration;
				Debug.Log("Up Slope");
			}else{
				newVelocityX += flatAcceleration;
			}
			newVelocityX = Mathf.Clamp(newVelocityX, -maxSpeed, maxSpeed);
		}else if(velocity.x != 0){
			//int modifier = velocity.x > 0 ? -1 : 1;
			//newVelocityX = flatAcceleration * modifier;
		}

		velocity = new Vector2(newVelocityX, velocity.y);

		if(velocity.x != 0){
			Vector2 startPoint =  new Vector2(box.center.x, box.yMin);
			Vector2 endPoint =  new Vector2(box.center.x, box.yMax);

			RaycastHit2D[] hitInfos =  new RaycastHit2D[horizontalRays];
			int amountConnected = 0;
			float lastFraction = 0;	

			float sideRayLength =  box.width /2 + Mathf.Abs(newVelocityX *Time.deltaTime);
			Vector2 direction = newVelocityX > 0 ? Vector2.right : - Vector2.right;
			bool connected = false;

			for(int i = 0; i < horizontalRays; i++){
				float lerpAmount = (float) i / (float) (horizontalRays -1);
				Vector2 origin = Vector2.Lerp(startPoint, endPoint, lerpAmount);
				hitInfos[i] = Physics2D.Raycast(origin,direction, sideRayLength, whatIsFloor);

				if(hitInfos[i].fraction > 0){
					connected = true;
					if(lastFraction >0){
						float angle = Vector2.Angle(hitInfos[i].point - hitInfos[i-1].point, Vector2.right);

						if(Mathf.Abs(angle -90) < angleLeeway){
							transform.Translate(direction * (hitInfos[i].fraction * sideRayLength - box.width /2));
							velocity = new Vector2(0, velocity.y);
							slope = hitInfos[i].transform.tag;
							break;
						}
						amountConnected++;
						lastFraction = hitInfos[i].fraction;
					}
					break;
				}
				//slope = hitInfos[i].transform.tag;
			}
		}

	}
}
