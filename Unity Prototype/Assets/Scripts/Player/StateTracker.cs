﻿using UnityEngine;
using System.Collections;
//using UnityEditor;

public class StateTracker : MonoBehaviour {
	
	#region Internal Types
	
	public enum FacingDirection {left, right};
	
	#endregion
	

	#region Variables
	
	public bool moving;
	public bool idle;
	public bool onGround;
	public bool isDiving;
	public bool takingDamage;
	public bool isResetting;
	public bool canReset= false;
	public bool recentlyReset = false;
	public bool isJumping = false;
	public bool isWallSliding =false;

	public FacingDirection moveDirection;
	
	private RunnerMovement playerMovement;
	private Transform parent;

	[HideInInspector] public Animator playerAnim;
	
	#endregion

	
	#region Contrutor Methods
	
	// default constructor
	public StateTracker()
	{
		moving =  false ;
		idle = false;
		onGround = false;
		isDiving = false;
		takingDamage = false;
		moveDirection = FacingDirection.right;
	}
	
	#endregion
	
	#region MonoBehaviour
	// Use this for initialization
	void Start () 
	{
		// Only use this technique of finding the highest most parent in the hierarchy
		// and then finding the right component in its childern, if there is only one
		// component of it in the entire game object. This automates the system 
		parent = transform;
		while (parent.parent != null)
		{
			parent = parent.parent;
		}
		
		playerAnim = GetComponent<Animator>();
	}
	
	
	// Update is called once per frame
	void Update () 
	{
		// call all of the state checks
		CheckMovingState();
		ManageXScale();
		playerAnim.SetBool("isGrounded", onGround);
		playerAnim.SetBool("isJumping", isJumping);
		playerAnim.SetBool("isDiving", isDiving);

		//CheckIdling();
	}
	#endregion
	
	
	#region State checking Methods
	void CheckMovingState()
	{
		// checks to see wheter there a velocity. If so then its moving
		if(Mathf.Abs(rigidbody2D.velocity.x) > 0.1f)
		{
			moving = true;

		}
		else
		{
			moving = false;
		}
		playerAnim.SetBool("isMoving", moving);
		playerAnim.SetFloat("hSpeed", rigidbody2D.velocity.x);
		playerAnim.SetFloat("vSpeed", rigidbody2D.velocity.y);}
	
	
	void CheckIdling()
	{
		if(!onGround && !moving && !takingDamage)
		{
			idle = true;
		}
		else
		{
			idle = false;
		}
	}

	void ManageXScale(){
		if(rigidbody2D.velocity.x > 0f){//Moving right
			Vector3 currentScale = transform.localScale;
			if(Mathf.Sign(currentScale.x) != 1) currentScale.x *=-1;  
			transform.localScale = currentScale;
		}else if(rigidbody2D.velocity.x < 0f){//Moving left
			Vector3 currentScale = transform.localScale;
			if(Mathf.Sign(currentScale.x) != -1) currentScale.x *=-1;  
			transform.localScale = currentScale;
		}
	}
	
	
	#endregion
}
