﻿using UnityEngine;
using System.Collections;

public class ResponsiveMusicSystem : MonoBehaviour {

	public float musicVolume;
	public bool useResponsiveSystem = false;
	public bool setSongForLevel;
	public AudioClip[] staticSong;
	public AudioClip setSong;
	//public AudioClip[] individualBeats;

	public GameObject[] songClips;
	public float[] songActivationSpeeds;
	private Rigidbody2D _PlayerMover;

	/*public bool songPart1;
	public bool songPart2;
	public bool songPart3;
	public bool songPart4;
	public bool songPart5;
	public bool songPart6;
	public bool songPart7;
	public bool songPart8;
	public bool songPart9;
	public bool songPart10;*/


	public float loadInDelay;

	//public Hashtable

	// Use this for initialization
	void Start () {
		audio.volume = musicVolume;
		if(!useResponsiveSystem){
			if(!setSongForLevel){
				audio.clip = staticSong[Mathf.RoundToInt(Random.Range( 0, staticSong.Length) )];
			}else{
				audio.clip = setSong;
			}
			audio.loop = true;
			audio.Play();
		}
		_PlayerMover = GameObject.FindGameObjectWithTag("Player").rigidbody2D;
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log("Sample rate: "+ AudioSettings.outputSampleRate);
		if(useResponsiveSystem) UseResponsiveSystem();

	}

	void UseResponsiveSystem(){
		for(int i = 0; i < songClips.Length; i++){
			if(_PlayerMover.velocity.x >= songActivationSpeeds[i] && !songClips[i].audio.isPlaying){
				//songClips[0].audio.volume = 1f;
				songClips[i].audio.timeSamples =songClips[0].audio.timeSamples;
				iTween.AudioTo(songClips[i], 1f, 1f, loadInDelay);
				songClips[i].audio.Play();				
			}else if(_PlayerMover.velocity.x < songActivationSpeeds[i] && songClips[0].audio.volume > 0f){
				songClips[i].audio.volume = 0f;
				//iTween.AudioTo(songClips[0], 0f, 1f, 1f);
				if(songClips[i].audio.volume <= 0f)songClips[i].audio.Stop();
			}
		}
		/*if(songPart1 && !songClips[0].audio.isPlaying){
			//songClips[0].audio.volume = 1f;
			iTween.AudioTo(songClips[0], 1f, 1f, loadInDelay);
			songClips[0].audio.Play();

		}else if(!songPart1 && songClips[0].audio.volume > 0f){
			songClips[0].audio.volume = 0f;
			//iTween.AudioTo(songClips[0], 0f, 1f, 1f);
			if(songClips[0].audio.volume <= 0f)songClips[0].audio.Stop();
		}

		if(songPart2 && !songClips[1].audio.isPlaying){
			//songClips[1].audio.volume = 1f;
			iTween.AudioTo(songClips[1], 1f, 1f, loadInDelay);
			songClips[1].audio.timeSamples =songClips[0].audio.timeSamples;
			songClips[1].audio.Play();
		}else if(!songPart2 && songClips[1].audio.volume > 0f){
			songClips[1].audio.volume = 0f;
			//iTween.AudioTo(songClips[1], 0f, 1f, 1f);
			if(songClips[1].audio.volume <= 0f)songClips[1].audio.Stop();
		}

		if(songPart3 && !songClips[2].audio.isPlaying){
			//songClips[2].audio.volume = 1f;
			iTween.AudioTo(songClips[2], 1f, 1f, loadInDelay);
			songClips[2].audio.timeSamples =songClips[0].audio.timeSamples;
			songClips[2].audio.Play();
		}else if(!songPart3 && songClips[2].audio.volume > 0f){
			songClips[2].audio.volume = 0f;
			//iTween.AudioTo(songClips[2], 0f, 1f, 1f);
			if(songClips[2].audio.volume <= 0f)songClips[2].audio.Stop();
		}*/
	}
}
