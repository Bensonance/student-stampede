﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Author: Ben
/// Sort layer script.
/// </summary>
public class SortLayerScript : MonoBehaviour {

	public bool sortParticles;
	public string sortingLayerStringParticles;
	public int sortingLayerNoParticles;


	public bool sortLights;
	public string sortingLayerStringLights;
	public int sortingLayerNoLights;

	public bool sortText;
	// Use this for initialization
	void Start () 
	{
		if(sortParticles){
			particleSystem.renderer.sortingLayerName = sortingLayerStringParticles;
			particleSystem.renderer.sortingOrder = sortingLayerNoParticles;
		}

		if(sortText){
			renderer.sortingLayerName = "UI";

		}

		/*if(sortLights){
			light.renderer.sortingLayerName = sortingLayerStringLights;
			light.renderer.sortingOrder = sortingLayerNoLights;
		}*/
	}

}
