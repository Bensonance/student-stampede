﻿using UnityEngine;
using System.Collections;

public class SoundTracker : MonoBehaviour {

	public AudioClip[] jumpSounds;
	public AudioClip[] girlSounds;
	public AudioClip[] kuduSounds;
	private AudioClip jumpSoundPrior;//Jump sound played during the last jump
	public float jumpSoundVolume;
	public AudioClip runSound;

	public float[] runSoundTempos;
	public float runSoundVolume;
	public AudioClip fallOffScreen;
	public AudioClip fallOnScreen;

	private float audioStartPitch;
	private RunnerMovement _player;

	public RuntimeAnimatorController player1;
	public RuntimeAnimatorController player2;
	public RuntimeAnimatorController player3;
	/*States*/

	// Use this for initialization
	void Start () {
		audioStartPitch = audio.pitch;
		_player = GetComponent<RunnerMovement>();
	}
	
	// Update is called once per frame
	void Update () {
		if(audio.clip == runSound)audio.pitch = CheckRunTempo(_player.rigidbody2D.velocity.x);
	}

	public void PlayJumpSound(){
		if(GetComponent<Animator>().runtimeAnimatorController == player1){
			int selArr = Mathf.RoundToInt(Random.Range(0f, jumpSounds.Length-0.6f));
			audio.clip = jumpSounds[selArr];
			while(audio.clip == jumpSoundPrior){
				selArr = Mathf.RoundToInt(Random.Range(0f, jumpSounds.Length-0.6f));
				audio.clip = jumpSounds[selArr];
			}
		}else 
		if(GetComponent<Animator>().runtimeAnimatorController == player3){

		int selArr = Mathf.RoundToInt(Random.Range(0f, girlSounds.Length-0.6f));
		audio.clip = girlSounds[selArr];
		while(audio.clip == jumpSoundPrior){
			selArr = Mathf.RoundToInt(Random.Range(0f, girlSounds.Length-0.6f));
			audio.clip = girlSounds[selArr];
		}

		}else if(GetComponent<Animator>().runtimeAnimatorController == player2){

			int selArr = Mathf.RoundToInt(Random.Range(0f, kuduSounds.Length-0.6f));
			audio.clip = kuduSounds[selArr];
			while(audio.clip == jumpSoundPrior){
				selArr = Mathf.RoundToInt(Random.Range(0f, kuduSounds.Length-0.6f));
				audio.clip = kuduSounds[selArr];
			}
		}

		audio.pitch = audioStartPitch;
		audio.volume = jumpSoundVolume;
		audio.Play();
		audio.loop  =false;
		jumpSoundPrior = audio.clip;
	}

	public void PlayRunSound(float runSpeed){
		if(audio.clip != runSound || !audio.isPlaying){
			audio.clip = runSound;
			audio.volume = runSoundVolume;
			audio.Play();
			audio.loop  =true;
			audio.pitch = CheckRunTempo(runSpeed);
		}
	}

	float CheckRunTempo(float runSpeed1){
		Animator anim = GetComponent<Animator>();
		if(runSpeed1>= 1f && runSpeed1< 30f){
			return runSoundTempos[0];
		}else if(runSpeed1 >= 30f && runSpeed1 <45f){
			return runSoundTempos[1];
		}else if(runSpeed1 >= 45f && runSpeed1 <60f){
			return runSoundTempos[2];
		}else if(runSpeed1 >= 60f && runSpeed1 <75f){
			return runSoundTempos[3];
		}else if(runSpeed1 >= 90f){
			return runSoundTempos[4];
		}else{
			return audioStartPitch;
		}
	}
}
