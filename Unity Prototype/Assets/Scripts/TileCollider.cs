﻿using UnityEngine;
using System.Collections;

public class TileCollider : MonoBehaviour {

	public GameObject spriteToTile;

	private float startX;
	private float endX;

	private float currentX;
	private float currentY;

	private float startY;
	private float endY;

	public bool tileVertically;

	public bool mustDestroy = true;
	public bool tileCollidable;

	public GameObject leftObject;
		private float leftObjectWidth;
	public GameObject midObject;
		private float midObjectWidth;
	public GameObject rightObject;
		private float rightObjectWidth;

	public bool tileAtAngle =false;
	public GameObject TileUp;
	public GameObject TileDown;
	private float width;

	public int spriteLayerOrderOffset = 0;


	public GameObject objToParentTilesTo;
	public string objParentName;

	// Use this for initialization
	void Start () {
		if(objToParentTilesTo == null)objToParentTilesTo = GameObject.Find(objParentName);
		if(tileAtAngle){
			CreateAngleTiles();
		}else{
			CreateTiles();
		}
		/*while(currentX < endX){
			Debug.Log("Xrun");

			if(tileVertically){
				while(currentY > endY){
					Debug.Log("Yrun");
					Vector3 chosenVector = new Vector3(currentX, currentY, 0f);
					Instantiate(spriteToTile, chosenVector, transform.rotation);
					currentY-= 3.2f;
				}
			}else{
				Vector3 chosenVector = new Vector3(currentX, currentY, 0f);
				Instantiate(spriteToTile, chosenVector, transform.rotation);
			}
			currentY = startY;
			currentX += 3.2f;
		}*/

		if(mustDestroy)GameObject.Destroy(gameObject);
		GetComponent<SpriteRenderer>().enabled = false;

	}

	void CreateTiles(){
		GetTileWidths();

		Debug.Log("Width: " + collider2D.bounds.max.x);
		Debug.Log("Calculation: " + (collider2D.bounds.max.x - (leftObjectWidth+rightObjectWidth)));


		int tileNo = Mathf.RoundToInt( ( collider2D.bounds.size.x- (leftObjectWidth+rightObjectWidth))/midObjectWidth );

		Debug.Log(tileNo);
		startX = collider2D.bounds.min.x;
		startY = collider2D.bounds.max.y;

		
		GameObject newTile = (GameObject)Instantiate(leftObject, new Vector3(startX, startY, 0f), transform.rotation);
		if(!tileCollidable)newTile.collider2D.enabled = false;
		if(objToParentTilesTo != null)newTile.transform.parent = objToParentTilesTo.transform;
		newTile.GetComponent<SpriteRenderer>().sortingOrder = spriteLayerOrderOffset;
		startX+= leftObjectWidth;
		
		for(int i = 0; i < tileNo; i++){
			newTile = (GameObject)Instantiate(midObject, new Vector3(startX, startY, 0f), transform.rotation);
			if(!tileCollidable)newTile.collider2D.enabled = false;
			if(objToParentTilesTo != null)newTile.transform.parent = objToParentTilesTo.transform;
			newTile.GetComponent<SpriteRenderer>().sortingOrder = spriteLayerOrderOffset;
			startX += midObjectWidth;
		}
		
		newTile = (GameObject)Instantiate(rightObject, new Vector3(startX, startY, 0f), transform.rotation);
		if(!tileCollidable)newTile.collider2D.enabled = false;
		if(objToParentTilesTo != null)newTile.transform.parent = objToParentTilesTo.transform;
		newTile.GetComponent<SpriteRenderer>().sortingOrder = spriteLayerOrderOffset;
	}

	void CreateAngleTiles(){
		GameObject widthObj = (GameObject)Instantiate(TileUp);
		width = widthObj.collider2D.bounds.size.x;
		float height = widthObj.collider2D.bounds.size.y;
		Vector3 startPos = new Vector3(Mathf.Sign(transform.localEulerAngles.z ) == 1 ? collider2D.bounds.max.x : collider2D.bounds.min.x, collider2D.bounds.max.y, 0f);
		float decMod = Mathf.Sign(transform.localEulerAngles.z ) == 1? 1.1f : -1.1f;

		int tileNo = Mathf.RoundToInt( ( collider2D.bounds.size.x)/width );
		GameObject chosen = Mathf.Sign(transform.localEulerAngles.z ) == 1? TileUp: TileDown;
		startX = collider2D.bounds.min.x;
		float mod = Mathf.Sign(transform.localEulerAngles.z ) == 1? 2f: -2f;
		startY = collider2D.bounds.max.y-height+mod;

		for(int i = 0; i < tileNo; i++){
			Quaternion goalQuat = transform.rotation;
			goalQuat.z = 0f;
			goalQuat = new Quaternion(0f, 0f, 0f, 0f);
			GameObject newTile = (GameObject)Instantiate(chosen, new Vector3(startX, startY, 0f), goalQuat );
			if(!tileCollidable)newTile.collider2D.enabled = false;
			if(objToParentTilesTo != null)newTile.transform.parent = objToParentTilesTo.transform;
			newTile.GetComponent<SpriteRenderer>().sortingOrder = spriteLayerOrderOffset;
			startX += width * Mathf.Sign(decMod);
			startY += decMod;
		}

		Destroy(widthObj);
	}


	void GetTileWidths(){
		//Left
		GameObject leftObjectTest = (GameObject)Instantiate(leftObject);
			leftObjectWidth = leftObjectTest.collider2D.bounds.size.x;
		Debug.Log("Left size: " + leftObjectWidth);
			Destroy(leftObjectTest);

		//Right
		GameObject rightObjectTest = (GameObject)Instantiate(rightObject);
			rightObjectWidth = rightObjectTest.collider2D.bounds.size.x;
		Debug.Log("Right size: " + rightObjectWidth);
			Destroy(rightObjectTest);

		//Middle
		GameObject midObjectTest = (GameObject)Instantiate(midObject);
			midObjectWidth = midObjectTest.collider2D.bounds.size.x;
		Debug.Log("mid size: " + midObjectWidth);
			Destroy(midObjectTest);
	}
}
