﻿using UnityEngine;
using System.Collections;

public class Utility_DestroyInPeriod : MonoBehaviour {

	public float destroyPeriod;
	private float currentTime;

	// Use this for initialization
	void Start () {
		currentTime = destroyPeriod;
	}
	
	// Update is called once per frame
	void Update () {
		if(currentTime> 0){
			currentTime -= Time.deltaTime;
		}else{
			Destroy(gameObject);
		}
	}
}
