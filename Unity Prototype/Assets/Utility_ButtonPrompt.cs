﻿using UnityEngine;
using System.Collections;

public class Utility_ButtonPrompt : MonoBehaviour {

	public bool pauseUntilFulfilled;
	public KeyCode buttonDesired;
	public string directive;
	public float heldDownPeriod;
	public float guiTextSizeMode;
	public GameObject prompt;
	private bool hasStarted = false;
	private float curTime;
	private bool isActivated = false;

	// Use this for initialization
	void Start () {
		prompt.guiText.pixelOffset = new Vector2(Screen.width/2, Screen.height/2);
		prompt.guiText.fontSize = Mathf.RoundToInt(Screen.width * guiTextSizeMode);
		prompt.guiText.text = directive + buttonDesired.ToString();
		prompt.guiText.enabled = false;
		prompt.transform.position = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
		prompt.guiText.pixelOffset = new Vector2(Screen.width/2, Screen.height/2);
		prompt.guiText.fontSize = Mathf.RoundToInt(Screen.width * guiTextSizeMode);
		if(isActivated){
			if(Input.GetKeyDown(buttonDesired)){
				if(heldDownPeriod > 0) 
				{
					if(!hasStarted)
					{
						curTime = heldDownPeriod;
						hasStarted = true;
					}
				}else{
					Continue();
				}
			}else if(Input.GetKeyUp(buttonDesired)){
				hasStarted = false;
				StopAllCoroutines();
			}

			if(hasStarted){
				if(curTime >0){
					curTime-= Time.unscaledDeltaTime;
				}else{
					Continue();
				}
			}
		}
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "Player")
		{
			if(pauseUntilFulfilled) Time.timeScale = 0;
			prompt.guiText.enabled = true;
			isActivated = true;
		}
	}

	IEnumerator ActivateDelay(float period)
	{
		yield return new WaitForSeconds(period);
		Continue();
	}

	void Continue()
	{
		Debug.Log("YEAHHHH");
		if(pauseUntilFulfilled)Time.timeScale = 1;
		Destroy(gameObject);
	}
}
