﻿using UnityEngine;
using System.Collections;

public class Utility_ManageGUIText : MonoBehaviour {


	public float guiTextSizeModTimeOnly;
	public bool isTexture = false;
	/*Tweening Stuff */
	private float newWidth;
	private float newHeight;

	public float startHeight;
	private float actualStartHeight;
	public float startWidth;
	private float actualStartWidth;

	public bool modPerc;

	//Textures
	private float newLeft;
	private float newTop;
	private float newTextWidth;
	private float newTextHeight;
	
	public float startDivLeft;
	public float startDivTop;
	public float startDivWidth;
	public float startDivHeight;

	private float startLeftPos;
	private float startTopPos;
	private float startWidthPos;
	private float startHeightPos;

	public float goalDivLeft;
	public float goalDivTop;
	public float goalDivWidth;
	public float goalDivHeight;

	private float goalLeftPos;
	private float goalTopPos;
	private float goalWidthPos;
	private float goalHeightPos;

	public bool liveUp;
	public bool startPos;

	public bool positionOnStart;
	// Use this for initialization
	void Start () {
		actualStartWidth = startWidth;
		actualStartHeight = startHeight;

		startLeftPos = Screen.width/startDivLeft;
		startTopPos = Screen.height/startDivTop;
		startWidthPos = Screen.width/startDivWidth;
		startHeightPos = Screen.height/startDivHeight;

		goalLeftPos = Screen.width/goalDivLeft;
		goalTopPos = Screen.height/goalDivTop;
		goalWidthPos = Screen.width/goalDivWidth;
		goalHeightPos = Screen.height/goalDivHeight;

		goalWidthPos = startWidthPos;
		goalHeightPos = startHeightPos;

		newLeft = startLeftPos;
		newTop = startTopPos;
		newTextWidth = startWidthPos;
		newTextHeight = startHeightPos;

		if(isTexture){

		}else{
			guiText.fontSize = Mathf.RoundToInt(Screen.height * guiTextSizeModTimeOnly);
		}

		transform.position = Vector3.zero;

		if(positionOnStart){
			if(isTexture){
				guiTexture.pixelInset = new Rect(newLeft, newTop, newTextWidth, newTextHeight);
			}else{
				guiText.pixelOffset = new Vector2(newWidth, newHeight);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(isTexture){
			guiTexture.pixelInset = new Rect(newLeft, newTop, newTextWidth, newTextHeight);
		}else{
			if(modPerc){
				newWidth = Screen.width/startWidth;
				newHeight = Screen.height/startHeight;
			}
			guiText.pixelOffset = new Vector2(newWidth, newHeight);
		}

		if(liveUp )LiveUpdate();
	}



	public void TextureTweenToGoal(float time){
		//ChangeLeft(goalLeftPos, iTween.EaseType.easeInExpo, time);
   		ChangeTop(goalTopPos, iTween.EaseType.easeInExpo, time);
		//ChangeWidth(goalWidthPos, iTween.EaseType.easeInExpo, time);
		//ChangeHeight(goalHeightPos, iTween.EaseType.easeInExpo, time);
	}

	public void SetupPosition(float width, float height){
		startWidth = width;
		startHeight = height;
		newWidth = startWidth;
		newHeight = startHeight;

		newLeft = startLeftPos;
		newTop = startTopPos;
		newTextWidth = startWidthPos;
		newTextHeight = startHeightPos;

		if(isTexture){
			guiTexture.pixelInset = new Rect(newLeft, newTop, newTextWidth, newTextHeight);
		}else{
			guiText.pixelOffset = new Vector2(/*Screen.width/2+(Screen.width/16)*/ startWidth, /*Screen.height/5*/ startHeight);
		}
	}

	void LiveUpdate(){
		if(isTexture){
			if(startPos){
				newLeft = startLeftPos;
				newTop = startTopPos;
				newTextWidth = startWidthPos;
				newTextHeight = startHeightPos;
			}else{
				goalLeftPos = Screen.width/goalDivLeft;
				goalTopPos = Screen.height/goalDivTop;
				goalWidthPos = Screen.width/goalDivWidth;
				goalHeightPos = Screen.height/goalDivHeight;

				newLeft = goalLeftPos;
				newTop = goalTopPos;
				newTextWidth = goalWidthPos;
				newTextHeight = goalHeightPos;
			}
		}else{
			if(startPos){
				startWidth = actualStartWidth;
				startHeight = actualStartHeight;
			}else{
	
			}
		}

	}

	#region MovementTweens

	public void ChangeTextWidth(float newLeftGoal, iTween.EaseType easeType, float period){
		iTween.ValueTo( gameObject, iTween.Hash(
			"from", startWidth,
			"to", newLeftGoal,
			"time", period,
			"onupdatetarget", gameObject,
			"onupdate", "tweenOnUpdateCallBackTextWidth",
			"easetype", easeType
			)
		               );
	}
	
	void tweenOnUpdateCallBackTextWidth( int newValue )
	{
		newWidth = newValue;
	}


	public void ChangeTextHeight(float newTopGoal, iTween.EaseType easeType, float period){
		iTween.ValueTo( gameObject, iTween.Hash(
			"from", startHeight,
			"to", newTopGoal,
			"time", period,
			"onupdatetarget", gameObject,
			"onupdate", "tweenOnUpdateCallBackTextHeight",
			"easetype", easeType
			)
		               );
	}
	
	void tweenOnUpdateCallBackTextHeight( int newValue )
	{
		newHeight = newValue;
	}


	#endregion MovementTweens

	#region TextureTweens
	public void ChangeLeft(float newLeftGoal, iTween.EaseType easeType, float period){
		iTween.ValueTo( gameObject, iTween.Hash(
			"from", startLeftPos,
			"to", newLeftGoal,
			"time", period,
			"onupdatetarget", gameObject,
			"onupdate", "tweenOnUpdateCallBackChangeLeft",
			"easetype", easeType
			)
		               );
	}
	
	void tweenOnUpdateCallBackChangeLeft( int newValue )
	{
		newLeft = newValue;
	}

	public void ChangeTop(float newTopGoal, iTween.EaseType easeType, float period){
		iTween.ValueTo( gameObject, iTween.Hash(
			"from", startTopPos,
			"to", newTopGoal,
			"time", period,
			"onupdatetarget", gameObject,
			"onupdate", "tweenOnUpdateCallBackChangeTop",
			"easetype", easeType
			)
		               );
	}
	
	void tweenOnUpdateCallBackChangeTop( int newValue )
	{
		newTop = newValue;
	}

	public void ChangeWidth(float newWidthGoal, iTween.EaseType easeType, float period){
		iTween.ValueTo( gameObject, iTween.Hash(
			"from", startWidthPos,
			"to", newWidthGoal,
			"time", period,
			"onupdatetarget", gameObject,
			"onupdate", "tweenOnUpdateCallBackChangeTextWidth",
			"easetype", easeType
			)
		               );
	}
	
	void tweenOnUpdateCallBackChangeTextWidth( int newValue )
	{
		newTextWidth = newValue;
	}

	public void ChangeHeight(float newHeightGoal, iTween.EaseType easeType, float period){
		iTween.ValueTo( gameObject, iTween.Hash(
			"from", startHeightPos,
			"to", newHeightGoal,
			"time", period,
			"onupdatetarget", gameObject,
			"onupdate", "tweenOnUpdateCallBackChangeTextHeight",
			"easetype", easeType
			)
		               );
	}
	
	void tweenOnUpdateCallBackChangeTextHeight( int newValue )
	{
		newTextHeight = newValue;
	}
	#endregion TextueTweens
}
