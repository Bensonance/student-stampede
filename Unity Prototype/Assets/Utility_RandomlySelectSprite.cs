﻿using UnityEngine;
using System.Collections;

public class Utility_RandomlySelectSprite : MonoBehaviour {

	public Sprite[] spriteSelection;

	// Use this for initialization
	void Start () {
		GetComponent<SpriteRenderer>().sprite = RandomlySelectSprite(spriteSelection);
	}

	Sprite RandomlySelectSprite(Sprite[] sprites){
		Sprite chosenSprite = sprites[Mathf.RoundToInt(Random.Range(0, sprites.Length))];
		return chosenSprite;
	}
}
