﻿using UnityEngine;
using System.Collections;

public class Utility_ShakeOnStart : MonoBehaviour {

	public AudioClip shakeSound;
	public float shakeSoundVolume;
	public float shakeIntensity;
	public float shakePeriod;

	// Use this for initialization
	void Start () {
		Camera.main.GetComponent<SinglePlayerCamera>().ShakeScreen(shakeIntensity, shakePeriod);
		if(shakeSound != null) AudioSource.PlayClipAtPoint(shakeSound, transform.position, shakeSoundVolume);
	}
}
