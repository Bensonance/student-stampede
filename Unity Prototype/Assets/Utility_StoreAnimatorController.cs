﻿using UnityEngine;
using System.Collections;

public class Utility_StoreAnimatorController : MonoBehaviour {

	public RuntimeAnimatorController playerChosenChar;

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(gameObject);
		if(GameObject.Find("Runner Player 1") != null){
			GameObject.Find("Runner Player 1").GetComponent<Animator>().runtimeAnimatorController = playerChosenChar;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnLevelWasLoaded(int level){
		if(GameObject.Find("Runner Player 1") != null){
			GameObject.Find("Runner Player 1").GetComponent<Animator>().runtimeAnimatorController = playerChosenChar;
		}
	}
}
