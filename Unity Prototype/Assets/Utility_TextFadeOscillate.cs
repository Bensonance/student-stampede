﻿using UnityEngine;
using System.Collections;

public class Utility_TextFadeOscillate : MonoBehaviour {

	public float changeSpeed = 0f;
	private float curA = 0f;
	public bool startOff;
	public float minAlpha;
	// Use this for initialization
	void Start () {
		if(startOff){
			curA = 0f;
		}else{
			curA = 1f;
			changeSpeed *= -1;
		}
	}
	
	// Update is called once per frame
	void Update () {

		if(curA < minAlpha){
			curA = minAlpha;
			changeSpeed *= -1;
		}else if(curA >1){
			curA = 1;
			changeSpeed *= -1;
		}

		curA += changeSpeed;

		Color col = guiText.material.color;
		col.a = curA;
		guiText.material.color = col;
	}
}
